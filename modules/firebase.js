var admin = require('firebase-admin');
var serviceAccount = require('../ressources/serviceAccountKey.json')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    storageBucket: "beparked-199409.appspot.com"
});

var bucket = admin.storage().bucket();

/*
upload an image
 */
async function upload(file) {
    return new Promise((resolve, reject) => {
        //NEED TO MODIFY THE FILE NAME ACCORDING TO ID AND MULTIPLE PARAMETERS
        var blob = bucket.file(file.originalname);
        blob.save(new Buffer(file.buffer)).then(() => {
            resolve("image uploaded successfully");
        }).catch((err) => {
            reject(err);
        })
    })
}

/*
delete a picture
 */
async function deleteFile(name) {
    return new Promise((resolve, reject) => {
        var file = bucket.file(name);
        file.delete().then(() => {
            resolve("picture deleted successfully");
        })
    })
};

exports.deleteFile = deleteFile;
exports.upload = upload