const nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'beparkedhevs@gmail.com',
        pass: 'beparked123'
    }
});

/*
send an email to the client to confirm his reservation
 */
function confirmBooking(email, firstname, date, startHour, endHour, address, latitude, longitude, placeNumber, description, totalPrice) {
    return new Promise(function (resolve, reject) {

        var greetings = 'Bonjour ' + firstname + ',';
        var table =
            '<p><b>' + date + '</b>, de ' + startHour + ' à ' + endHour + '</p>' +
            '<p><b>' + address + ' N°' + placeNumber + '</b>, ' + latitude + ', ' + longitude + '</p>' +
            '<p>' + description + '</p>' +
            '<p><b>Prix total : </b>' + totalPrice + '</p>';
        var firstMessage = 'Nous vous confirmons la réservation suivante :' + table;
        var secondMessage = 'Merci pour votre réservation et à bientôt sur BeParked !';

        var message = buildMessage('', greetings, firstMessage, secondMessage);

        var mailOptions = {
            from: 'beparkedhevs@gmail.com',
            to: email,
            subject: 'Confirmation de réservation',
            html: message
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                reject(error);

            } else {
                console.log('Email sent: ' + info.response);
                resolve("Confirmation mail sent");
            }
        });
    })
}

/*
send an email to confirm the publication of the owner
 */
function confirmSpotCreation(email, firstname, address, latitude, longitude, city, placeNumber, description, price) {
    return new Promise(function (resolve, reject) {

        var greetings = 'Bonjour ' + firstname + ',';
        var table =
            '<p><b>' + address + ' N°' + placeNumber + '</b>, ' + latitude + ', ' + longitude + '</p>' +
            '<p>' + city + '</p>' +
            '<p>' + description + '</p>' +
            '<p><b>Prix / h : </b>' + price + '</p>';
        var firstMessage = 'Nous vous confirmons que la place suivante a bien été mise en ligne :' + table;
        var secondMessage = 'N\'oubliez pas d\'ajouter les disponibilités de votre place sous l\'onglet « Disponibilités ».<br>Merci pour votre proposition et à bientôt sur BeParked !';

        var message = buildMessage('', greetings, firstMessage, secondMessage);

        var mailOptions = {
            from: 'beparkedhevs@gmail.com',
            to: email,
            subject: 'Confirmation de création',
            html: message
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                reject(error);

            } else {
                console.log('Email sent: ' + info.response);
                resolve("Confirmation mail sent");
            }
        });
    })
}

/*
send a confirmation email to the new customer registered in the website
 */
function confirmRegister(email, firstname) {
    return new Promise(function (resolve, reject) {

        var greetings = 'Bonjour ' + firstname + ',';
        var firstMessage = "Nous vous confirmons votre inscription sur BeParked. Vous pouvez désormais proposer ou réserver des places de stationnement. Vous avez également accès à vos données personnelles, que vous pouvez modifier à tout moment depuis votre profil.";
        var secondMessage = 'Merci pour votre inscription et à bientôt sur BeParked !';

        var message = buildMessage('', greetings, firstMessage, secondMessage);

        var mailOptions = {
            from: 'beparkedhevs@gmail.com',
            to: email,
            subject: 'Confirmation d\'inscription',
            html: message
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                reject(error);

            } else {
                console.log('Email sent: ' + info.response);
                resolve("Confirmation mail sent");
            }
        });
    })
}

/*
Send an email to the administrator with the customers question
 */
function contact(name, email, subject, text) {
    return new Promise(function (resolve, reject) {

        var greetings = 'Réception du message suivant de : ' + name + " " + email;
        var firstMessage = text;
        var secondMessage = '';

        var message = buildMessage('', greetings, firstMessage, secondMessage);

        var mailOptions = {
            from: email,
            to: 'beparkedhevs@gmail.com',
            subject: subject,
            html: message
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                reject(error);

            } else {
                console.log('Email sent: ' + info.response);
                resolve("Confirmation mail sent");
            }
        });
    })
}

/*
construct the message with the parameters
 */
function buildMessage(preheader, greetings, firstMessage, secondMessage) {
    //HEAD
    var head =
        '<head>\n' +
        '    <meta name="viewport" content="width=device-width">\n' +
        '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n' +
        '    <title>Simple Transactional Email</title>\n' +
        '    <style>\n' +
        '    @media only screen and (max-width: 620px) {\n' +
        '      table[class=body] h1 {\n' +
        '        font-size: 28px !important;\n' +
        '        margin-bottom: 10px !important;\n' +
        '      }\n' +
        '      table[class=body] p,\n' +
        '            table[class=body] ul,\n' +
        '            table[class=body] ol,\n' +
        '            table[class=body] td,\n' +
        '            table[class=body] span,\n' +
        '            table[class=body] a {\n' +
        '        font-size: 16px !important;\n' +
        '      }\n' +
        '      table[class=body] .wrapper,\n' +
        '            table[class=body] .article {\n' +
        '        padding: 10px !important;\n' +
        '      }\n' +
        '      table[class=body] .content {\n' +
        '        padding: 0 !important;\n' +
        '      }\n' +
        '      table[class=body] .container {\n' +
        '        padding: 0 !important;\n' +
        '        width: 100% !important;\n' +
        '      }\n' +
        '      table[class=body] .main {\n' +
        '        border-left-width: 0 !important;\n' +
        '        border-radius: 0 !important;\n' +
        '        border-right-width: 0 !important;\n' +
        '      }\n' +
        '      table[class=body] .btn table {\n' +
        '        width: 100% !important;\n' +
        '      }\n' +
        '      table[class=body] .btn a {\n' +
        '        width: 100% !important;\n' +
        '      }\n' +
        '      table[class=body] .img-responsive {\n' +
        '        height: auto !important;\n' +
        '        max-width: 100% !important;\n' +
        '        width: auto !important;\n' +
        '      }\n' +
        '    }\n' +
        '    @media all {\n' +
        '      .ExternalClass {\n' +
        '        width: 100%;\n' +
        '      }\n' +
        '      .ExternalClass,\n' +
        '            .ExternalClass p,\n' +
        '            .ExternalClass span,\n' +
        '            .ExternalClass font,\n' +
        '            .ExternalClass td,\n' +
        '            .ExternalClass div {\n' +
        '        line-height: 100%;\n' +
        '      }\n' +
        '      .apple-link a {\n' +
        '        color: inherit !important;\n' +
        '        font-family: inherit !important;\n' +
        '        font-size: inherit !important;\n' +
        '        font-weight: inherit !important;\n' +
        '        line-height: inherit !important;\n' +
        '        text-decoration: none !important;\n' +
        '      }\n' +
        '      .btn-primary table td:hover {\n' +
        '        background-color: #34495e !important;\n' +
        '      }\n' +
        '      .btn-primary a:hover {\n' +
        '        background-color: #34495e !important;\n' +
        '        border-color: #34495e !important;\n' +
        '      }\n' +
        '    }\n' +
        '    </style>\n' +
        '</head>';

    // GOOD BYE
    var goodBye = 'L’équipe BeParked';
    // COMPANY
    var company = '© 2018 BeParked, start-up issue du programme d\'entrepreneuriat Business eXperience de la HES-SO Valais-Wallis, en collaboration avec d\'autres écoles, notamment la Haute Ecole de Gestion de Genève';

    // BODY
    var body =
        '<body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">\n' +
        '    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">\n' +
        '      <tr>\n' +
        '        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>\n' +
        '        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">\n' +
        '          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">\n' +
        '\n' +
        '            <!-- START CENTERED WHITE CONTAINER -->\n' +
        '            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">' + preheader + '</span>\n' +
        '            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">\n' +
        '\n' +
        '              <!-- START MAIN CONTENT AREA -->\n' +
        '              <tr>\n' +
        '                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">\n' +
        '                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">\n' +
        '                    <tr>\n' +
        '                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">\n' +
        '                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">' + greetings + '</p>\n' +
        '                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">' + firstMessage + '</p>\n' +
        '                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">\n' +
        '                          <tbody>\n' +
        '                            <tr>\n' +
        '                              <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">\n' +
        '                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">\n' +
        '                                  <tbody>\n' +
        '                                    <tr>\n' +
        '                                    </tr>\n' +
        '                                  </tbody>\n' +
        '                                </table>\n' +
        '                              </td>\n' +
        '                            </tr>\n' +
        '                          </tbody>\n' +
        '                        </table>\n' +
        '                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">' + secondMessage + '</p>\n' +
        '                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"' + goodBye + '</p>\n' +
        '                      </td>\n' +
        '                    </tr>\n' +
        '                  </table>\n' +
        '                </td>\n' +
        '              </tr>\n' +
        '\n' +
        '            <!-- END MAIN CONTENT AREA -->\n' +
        '            </table>\n' +
        '\n' +
        '            <!-- START FOOTER -->\n' +
        '            <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">\n' +
        '              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">\n' +
        '                <tr>\n' +
        '                  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">\n' +
        '                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">' + company + '</span>\n' +
        '                    <br<a href="https://www.beparked.ch" style="text-decoration: underline; color: #999999; font-size: 12px; text-align: center;">www.beparked.ch</a>.\n' +
        '                  </td>\n' +
        '                </tr>\n' +
        '                <tr>\n' +
        '                  <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">\n' +
        '                    Powered by <a href="http://htmlemail.io" style="color: #999999; font-size: 12px; text-align: center; text-decoration: none;">HTMLemail</a>.\n' +
        '                  </td>\n' +
        '                </tr>\n' +
        '              </table>\n' +
        '            </div>\n' +
        '            <!-- END FOOTER -->\n' +
        '\n' +
        '          <!-- END CENTERED WHITE CONTAINER -->\n' +
        '          </div>\n' +
        '        </td>\n' +
        '        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>\n' +
        '      </tr>\n' +
        '    </table>\n' +
        '</body>';

    var message = '<html>' + head + body + '</html>';
    return message;


}

module.exports.confirmSpotCreation = confirmSpotCreation;
module.exports.confirmRegister = confirmRegister;
module.exports.confirmBooking = confirmBooking;
module.exports.contact = contact;
