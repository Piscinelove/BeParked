var dbSpots = require("../db/spot_db");
var dbTown = require('../db/town_db');
var dbBooking = require('../db/book_db');
var firebase = require('../modules/firebase');

/**
 *Get all spots from data layer
 */
async function getAllSpots() {
    var spots = await dbSpots.getAllSpots();
    return spots;
}

/*
get spot by his id
 */
async function getSpotById(idSpot) {
    //get the spot in the DB
    var spot = await dbSpots.getSpotById(idSpot);
    console.log(spot);
    //calculate amount of money earned
    var amount = 0;
    spot.Bookings.forEach(booking => {
        amount += booking.price;
    });
    spot.amount = Math.round(amount*100)/100;

    return spot;

}

/*
get all the available spots at a certain date between two hours
 */
async function getAllAvailableSpots(date, startHour, endHour, idUser) {
    try {
        var spots = await dbSpots.getAllAvailableSpots(date, startHour, endHour, idUser);
        spots = JSON.parse(JSON.stringify(spots));
        for (let i = 0; i < spots.length; i++) {
            var disponibilities = spots[i].Disponibilities;
            for (let j = 0; j < disponibilities.length; j++) {
                if (disponibilities[j].startHour <= startHour && disponibilities[j].endHour >= endHour && disponibilities[j].date == date) {
                    spots[i].target = disponibilities[j].id;

                    var bookings = disponibilities[j].Bookings;
                    if (bookings.length == 0)
                        spots[i].available = true;
                    else
                        for (let k = 0; k < bookings.length; k++) {
                            console.log(bookings.length);
                            if ((bookings[k].startHour >= endHour || bookings[k].endHour <= startHour))
                            {
                                console.log(disponibilities[j].startHour +" "+disponibilities[j].endHour);
                                console.log(bookings[k].startHour+" "+bookings[k].endHour);
                                spots[i].available = true;
                            }
                            else{
                                spots[i].available = false;
                                break;
                            }
                        }

                }
            }
        }


        return spots;
    } catch (error) {
        // ERROR 204 = NO-CONTENT
        throw new Error("204");
    }
}

/*
update a spot with a new number, latitude, longitude, address, description, price, town
 */
async function updateSpot(idSpot, number, latitude, longitude, address, description, price, town) {
    try {
        var town = await dbTown.getTownByName(town);
        var spot = await dbSpots.updateSpot(idSpot, number, latitude, longitude, address, description, price, town.id);
        return spot;
    } catch (error) {
        // ERROR 204 = NO-CONTENT
        throw new Error("204");
    }
}

/*
create a new spot with an address, town, longitude, latitude, number, price, description, file, idUser
 */
async function createSpot(address, town, longitude, latitude, number, price, description, file, idUser) {
    var town = await dbTown.getTownByName(town);
    var spot = await dbSpots.createSpot(number, latitude, longitude, address, description, price, idUser, town.id);
    spot = await dbSpots.updateSpotPicture(spot.id, file.originalname);
    //await firebase.upload(file);
    return spot;
}

/*
delete a spot by his id
 */
async function deleteSpot(id) {
    try {
        await dbBooking.getBookingsBySpot(id);
        //var picture = await dbSpots.getSpotPicture(id);
        //await firebase.deleteFile(picture);
        await dbSpots.deleteSpot(id);
        return "Place de parc supprimée !";
    } catch (error) {
        return error;
    }
}


exports.deleteSpot = deleteSpot;
exports.getAllAvailableSpots = getAllAvailableSpots;
exports.getAllSpots = getAllSpots;
exports.updateSpot = updateSpot;
exports.createSpot = createSpot;
exports.getSpotById = getSpotById;

//getAllAvailableSpots("2018-04-23", "09:00:00", "11:00:00", 1);