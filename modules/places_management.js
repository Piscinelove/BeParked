const cantonDB = require("../db/canton_db");
const townDB = require("../db/town_db");
const fs = require('fs');

/*
Add cantons and town in the database
 */
async function createTownsAndCantons() {
    return new Promise((resolve, reject) => {
        fs.readFile('ressources/towns.json', 'utf8', async function (err, data) {
            var obj = JSON.parse(data);
            var cantons = Object.keys(obj);
            for (var i = 0; i < cantons.length; i++) {
                var canton = await cantonDB.createCanton(cantons[i]);
                var towns = obj[cantons[i]]
                for (var j = 0; j < towns.length; j++) {
                    await townDB.createTown(towns[j].Town, canton.id);
                }
            }
            resolve();
        });
    })
}

exports.createTownsAndCantons = createTownsAndCantons;