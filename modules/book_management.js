const bookDB = require('../db/book_db');


/**
 * create a book with the correct price depending on the park duration
 * @param {*} date 
 * @param {*} startHour 
 * @param {*} endHour 
 * @param {*} spotPrice 
 * @param {*} idSpot 
 * @param {*} idDisponibility 
 * @param {*} idUser 
 */

/*
create a book with the parameters date, start/end hour, price, idSpot, idDispo, idUser
 */
async function createBook(date, startHour, endHour, finalPrice, idSpot, idDisponibility, idUser ){
    await bookDB.createBook(startHour, endHour, finalPrice, idUser, idSpot, idDisponibility);
}

exports.createBook = createBook;