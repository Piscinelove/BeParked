const userDB = require("../db/user_db");
const bcrypt = require('bcrypt');

/**
 * LOGIN FUNCTION
 * @param email
 * @param password
 * @returns {Promise<*>}
 */

/*
login with a correct email and password
 */
async function login(email, password) {

    try {
        var user = await userDB.getUserByEmail(email);
        var correctPassword = await bcrypt.compare(password, user.password_hash);

        if (correctPassword)
            return user;
        else {

            // ERROR 401 = UNAUTHORIZED
            throw new Error('401');
        }
    }
    catch (error) {

        // ERROR 401 = UNAUTHORIZED
        throw new Error('401');

    }

}

module.exports.login = login;