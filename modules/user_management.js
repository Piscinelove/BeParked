var dbUsers = require("../db/user_db");
var dbTown = require("../db/town_db");
var dbBook = require("../db/book_db");
var dbDispo = require("../db/disponibility_db");
var dbSpot = require("../db/spot_db");


/*
Get an user by his id
 */
async function getUserById(id) {
    try {
        var users = await dbUsers.getUserById(id);

        return users;
    } catch (error) {
        // ERROR 204 = NO-CONTENT
        throw new Error("204");
    }
}

/*
update a user by a new firstname, lastname, address, town, phone, password, licensePlate, brand
 */
async function updateUser(idUser, firstname, lastname, address, town, phone, password, licensePlate, brand) {
    try {
        var town = await dbTown.getTownByName(town);

        if (password == "")
            var user = await dbUsers.updateUser(idUser, firstname, lastname, phone, address, brand, licensePlate, town.id);
        else
            var user = await dbUsers.updateUserAndPassword(idUser, firstname, lastname, phone, address, brand, licensePlate, password, town.id)

        return user;
    } catch (error) {
        // ERROR 204 = NO-CONTENT
        throw new Error("204");
    }
}

/**
 * Create new admin
 */
async function createUserAdmin(firstname, lastname, address, town, email, password, phone) {
    try {
        var town = await dbTown.getTownByName(town);
        var user = await dbUsers.createUser(firstname, lastname, email, phone, address, "", "", password, 1, town.id);

        return "Admin ajouté avec succès";
    }
    catch (error) {
        return error;
    }
}

/*
delete a User by his id
 */
async function deleteUser(idUser) {
    var userToDelete = await dbUsers.getUserById(idUser);

    //delete the user if admin
    if (userToDelete.idRole == 1) {
        await dbUsers.deleteUser(idUser);
        return "Admin supprimé avec succès!";
    }
    else {
        // get the current date
        var today = new Date();
        var day = today.getDate();
        var month = today.getMonth() + 1;
        var year = today.getFullYear();

        if (day < 10) {
            day = '0' + day
        }
        if (month < 10) {
            month = '0' + month
        }
        today = year + "-" + month + "-" + day;

        //get the bookings for this user
        var bookings = userToDelete.Bookings;
        for (let i = 0; i < bookings.length; i++) {
            if (bookings[i].Disponibility.date >= today) {
                return "Impossible de supprimer cet utilisateur, il y a des réservations à venir";
            }
        }

        //get the spots for this user
        var spots = userToDelete.Spots;
        for (let i = 0; i < spots.length; i++) {
            for (let j = 0; j < spots[i].Bookings.length; j++) {
                if (spots[i].Bookings[j].Disponibility.date >= today) {
                    return "Impossible de supprimer cet utilisateur, il y a des réservations à venir";
                }
            }
        }
        //delete the user
        await deleteUserAndRelations(userToDelete);
        return "Utilisateur supprimé avec succès !"
    }
}

/*
delete a user with his bookings, his spots with their bookings and disponibilities
 */
async function deleteUserAndRelations(user) {
    return new Promise(async (resolve, reject) => {
        //delete all user's bookings
        await dbBook.deleteBookingsOfUser(user.id);

        // delete all user's spot's and bookings and disponibilities
        user.Spots.forEach(async spot => {
            console.log(JSON.stringify(spot));
            spot.Bookings.forEach(async booking => {
                await dbBook.deleteBookingsOfSpot(spot.id);
            });
            spot.Disponibilities.forEach(async dispo => {
                await dbDispo.deleteDispo(dispo.id);
            });
            await dbSpot.deleteSpot(spot.id);
        })

        // //finally delete the user
        await dbUsers.deleteUser(user.id);
        resolve();
    })
}

exports.deleteUser = deleteUser;
exports.createUserAdmin = createUserAdmin;
exports.updateUser = updateUser;
exports.getUserById = getUserById;
exports.createUserAdmin = createUserAdmin;
