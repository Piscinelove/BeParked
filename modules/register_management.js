const userDB = require("../db/user_db");
const townDB = require("../db/town_db");


/**
 *
 * @param {*} firstname
 * @param {*} lastname
 * @param {*} address
 * @param {*} town
 * @param {*} email
 * @param {*} phone
 * @param {*} license
 * @param {*} brand
 * @param {*} password
 */

/*
register a new user in the db
 */
async function register(firstname, lastname, address, town, email, phone, license, brand, password) {
    console.log("REGISTER DEDANS");
    try {
        var city = await townDB.getTownByName(town);
        console.log(firstname, lastname, address, town, email, phone, license, brand, password, 2, city.id);
        console.log("fini town get db");
        var user = await userDB.createUser(firstname, lastname, email, phone, address, brand, license, password, 2, city.id);
        return user;
    }
    catch (error) {
        // ERROR 400 = BAD REQUEST
        throw new Error(400);
    }
}

module.exports.register = register;