var dbDispos = require("../db/disponibility_db");
var dbBooking = require('../db/book_db');

/*
update dispo by id with a new date, start/end hour
 */
async function updateDispo(id, date, startHour, endHour) {
    try {
        await dbDispos.updateDispo(id, date, startHour, endHour);
        var dispos = await dbDispos.getAllDisponibilitiesByDate("2018-04-01");
        console.log(dispos);

        return dispos;
    } catch (error) {
        // ERROR 204 = NO-CONTENT
        throw new Error("204");
    }
}

/*
delete dispo by his id
 */
async function deleteDispo(idDisponibility) {
    try {
        await dbBooking.getBookingsByDisponibility(idDisponibility);
        await dbDispos.deleteDispo(idDisponibility);
        return "Disponibilité supprimée !";
    } catch (error) {
        return error;
    }
}

/*
delete a dispo from a spot
 */
async function deleteDispoByIdSpot(idSpot) {
    try {
        await dbBooking.getBookingsByDisponibility(idDisponibility);
        await dbDispos.deleteDispoByIdSpot(idSpot);
        return "Disponibilité supprimée !";
    } catch (error) {
        return error;
    }
}

exports.deleteDispoByIdSpot = deleteDispoByIdSpot;
exports.deleteDispo = deleteDispo;
exports.updateDispo = updateDispo;