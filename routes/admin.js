var express = require('express');
var router = express.Router();
var spotManagement = require('../modules/spot_management');
var userManagement = require('../modules/user_management');
var dbSpot = require('../db/spot_db');
var dbTown = require('../db/town_db');
var dbUser = require('../db/user_db');
var dbBooking = require('../db/book_db')
var Multer = require('multer');
var firebase = require('../modules/firebase');
var mailManagement = require('../modules/mail_management');
var settingsDB = require('../db/setting_db');

var storage = Multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/spots');
    },
    filename: function (req, file, cb) {
        file.originalname = "spot-"+Date.now()+file.originalname;
        cb(null, file.originalname);
    }
})

const multer = Multer({
    storage: storage
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.redirect('admin/search');

});

/* GET for logout. */
router.get('/logout', function (req, res, next) {
    req.session.destroy();
    res.redirect('/login');
});

/* GET search page for spots. */
router.get('/search', function (req, res, next) {
    res.render('admin/search');
});

/** Render new spot place*/
router.get('/spot', function (req, res, next) {
    res.render('admin/spot');
});

/**Create new spot */
router.post('/spot', multer.single('picture'), async function (req, res, next) {
    //upload the picture to firebase and create the spot
    await spotManagement.createSpot(req.body.address, req.body.town, req.body.lng, req.body.lat, req.body.number, req.body.price, req.body.description, req.file, req.session.user.id);
    await mailManagement.confirmSpotCreation(req.session.user.email, req.session.user.firstname, req.body.address, req.body.lat, req.body.lng,req.body.town, req.body.number,req.body.description, req.body.price);
    res.redirect('/admin/profile')
})

/** Render new spot place*/
router.get('/spot', function (req, res, next) {
    res.render('admin/spot');
});

/* GET profile page
 * and send the user in parameter
 * */
router.get('/profile', async function (req, res, next) {
    var user = await userManagement.getUserById(req.session.user.id)
    var books = await dbBooking.getAllReservationsByUser(user.id);
    res.render('admin/profile', { user: user, books: books});
});

/* GET profile page
 * and send the user in parameter
 * */
router.get('/profile/id=:id', async function (req, res, next) {
    var user = await userManagement.getUserById(req.params.id);
    var books = await dbBooking.getAllReservationsByUser(user.id);
    res.render('admin/profile', { user: user, books: books});
});

/* GET profile-details page */
router.get('/profile/spot=:id', async function (req, res, next) {
    var spot = await spotManagement.getSpotById(req.params.id);
    res.render('admin/spot-details', { spot: spot });
});

/* GET all users for the admin part*/
router.get('/users', async function (req, res, next) {
    var users = await dbUser.getAllUsersExceptCurrent(req.session.user.id);
    res.render('admin/users', { users: users });
});

/*GET all bookings for the admin part*/
router.get('/bookings', async function (req, res, next) {
    var books = await dbBooking.getAllReservations();
    res.render('admin/bookings', { books: books });
});

/* GET user conditions*/
router.get('/conditions', async function (req, res, next) {
    var cgv = await settingsDB.getCGV();
    res.render('admin/conditions', {cgv:cgv.text});
});

/* GET SETTINGS VIEW */
router.get('/settings', async function (req, res, next) {
    res.render('admin/settings');
});




module.exports = router;