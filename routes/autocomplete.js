var express = require('express');
var router = express.Router();
var townDB = require('../db/town_db');

/* GET TOWNS AUTOCOMPLETE LIST */
router.get('/name=:input', async function(req, res, next) {

    var input = req.params.input;
    var towns = await townDB.getAllTowns(input);
    res.json(towns);
});

module.exports = router;
