var express = require('express');
var router = express.Router();
var loginManagement = require('../modules/login_management');
var registerManagement = require('../modules/register_management');
var mailManagement = require('../modules/mail_management');

/* GET home page. */
router.get('/', function(req, res, next) {
    if(req.session.user != null){
        res.redirect('../admin');
    } else {
        res.render('login/login');
    }
});

/* POST login. */
router.post('/', async function(req, res, next) {
    let email = req.body.email;
    let password = req.body.password;
    try {
        let user = await loginManagement.login(email, password);
        req.session.authenticated = true;
        req.session.user = user;
        res.redirect('../admin');
    }catch(error){

        res.render('login/login',{error:"e-mail ou mot de passe incorrect"});

    };
});

/* POST register. */
router.post('/register', async function(req, res, next) {
    let firstname = req.body.firstname;
    let lastname = req.body.lastname;
    let address = req.body.address;
    let city = req.body.town;
    let email = req.body.email;
    let phone = req.body.phone;
    let license = req.body.plate;
    let model = req.body.model;
    let password = req.body.password;

    try {
        await registerManagement.register(firstname, lastname, address, city, email, phone, license, model, password);
        await mailManagement.confirmRegister(email,firstname);
        res.redirect('/login');
    }catch(error){

    };
});

module.exports = router;