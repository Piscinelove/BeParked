var express = require('express');
var router = express.Router();
var settingsDB = require('../db/setting_db');

/* GET home page. */
router.get('/', async function(req, res, next) {
    var about = await settingsDB.getAbout();
    var cgv = await settingsDB.getCGV();
    res.render('home/index', {about:about.text,cgv:cgv.text});
});

module.exports = router;
