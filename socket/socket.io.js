var socket_io = require('socket.io');
var session = require('express-session');
var spot_management = require('../modules/spot_management');
var userManagement = require('../modules/user_management');
var bookManagement = require('../modules/book_management');
var mailManagement = require('../modules/mail_management');
var disponibilityManagement = require('../modules/disponibility_management');
var townDB = require('../db/town_db');
var spotDB = require('../db/spot_db');
var bookingDB = require('../db/book_db');
var disponibilityDB = require('../db/disponibility_db');
var settingsDB = require('../db/setting_db');

var io = socket_io();
var socket_api = {};

socket_api.io = io;

//waiting connections
io.on('connection', (socket) => {
    socket.on('disconnect', function () {
        console.log("THE USER #" + socket.id + " IS DISCONNECTED");
    });

    socket.on('available_spots_research', async (date, startHour, endHour, address, idUser) => {
        try {
            var spotsList = await spot_management.getAllAvailableSpots(date, startHour, endHour,idUser);
            socket.emit('available_spots_research_result', spotsList);
        } catch (error) {
            socket.emit('available_spots_research_result_fail', error.message);
        }
    })

    socket.on('towns_autocompletion', async (input) => {
        var towns = await townDB.getAllTowns(input);
        socket.emit('towns_autocompletion_result', towns);
    })

    socket.on('book_spot', async (bookObject) => {
        await bookManagement.createBook(bookObject.date, bookObject.startHour, bookObject.endHour, bookObject.finalPrice, bookObject.spot.id, bookObject.spot.target, bookObject.idUser);
        await mailManagement.confirmBooking(bookObject.email, bookObject.firstname, bookObject.date, bookObject.startHour, bookObject.endHour, bookObject.spot.address, bookObject.spot.latitude, bookObject.spot.longitude, bookObject.spot.number, bookObject.spot.description, bookObject.finalPrice);
        socket.emit('book_spot_result');
    })

    socket.on('update_user', async (idUser, firstname, lastname, address, town, phone, password, licensePlate, brand) => {
        var user = await userManagement.updateUser(idUser, firstname, lastname, address, town, phone, password, licensePlate, brand);
        socket.emit("update_user_result", user);
    })

    socket.on('update_spot', async (idSpot, address, town, longitude, latitude, number, price, description) => {
        var spot = await spot_management.updateSpot(idSpot, number, latitude, longitude, address, description, price, town);
        socket.emit('update_spot_result', spot);
    });

    socket.on('delete_spot', async (idSpot) => {
        var result = await spot_management.deleteSpot(idSpot);
        console.log("RESULT : " + result);
        socket.emit("delete_spot_result", result);
    })

    socket.on('add_disponibility_spot', async (idSpot, date, startHour, endHour, frequency) => {
        try {
            await disponibilityDB.createDisponibility(date, startHour, endHour, idSpot, frequency);
            socket.emit('add_disponibility_spot_result', "Disponibilité ajouté avec succès");
        } catch (error) {
            socket.emit('add_disponibility_spot_result', error);
        }
    })

    socket.on('get_spot_details', async (idSpot) => {
        var spot = await spot_management.getSpotById(idSpot);
        socket.emit('get_spot_details_result', spot);
    })

    socket.on('delete_booking', async (idBooking) => {
        await bookingDB.deleteBooking(idBooking)
        socket.emit('delete_booking_result', "Booking deleted successfully !");
    })

    socket.on('delete_disponibility', async (idDisponibility) => {
        var result = await disponibilityManagement.deleteDispo(idDisponibility);
        socket.emit('delete_disponibility_result', result);
    })

    socket.on('create_admin', async (firstname, lastname, address, town, email, phone, password) => {
        var result = await userManagement.createUserAdmin(firstname, lastname, address, town, email, password, phone);
        socket.emit('create_admin_result', result);
    })

    socket.on('delete_user', async (idUser) => {
        var result = await userManagement.deleteUser(idUser);
        socket.emit('delete_user_result', result);
    })

    socket.on('contact_form', async (name, email, subject, message) => {
        await mailManagement.contact(name, email, subject, message);
        socket.emit('contact_form_result');
    })

    socket.on('save_about_text', async (text) => {
        var about = await settingsDB.updateAbout(text);
        socket.emit('save_about_text_result',about.text);
    })

    socket.on('get_about_text', async () => {
        var about = await settingsDB.getAbout();
        socket.emit('get_about_text_result', about.text);
    })

    socket.on('save_conditions_text', async (text) => {
        var cgv = await settingsDB.updateCGV(text);
        socket.emit('save_about_text_result', cgv.text);
    })

    socket.on('get_conditions_text', async () => {
        var conditions = await settingsDB.getCGV();
        socket.emit('get_conditions_text_result', conditions.text);
    })

});

module.exports = socket_api;