const assert = require('assert');
const models = require("../models");
const townDB = require("../db/town_db");
const cantonDB = require("../db/canton_db");
const userDB = require("../db/user_db");
const roleDB = require("../db/role_db");
const spotDB = require('../db/spot_db');
const disponibilityDB = require('../db/disponibility_db');
const bcrypt = require('bcrypt');

/**
 * Unit Test - Canton Creation
 */
describe('CREATE CANTON DATABASE', () => {
    it('SHOULD CREATE A CANTON', async () => {
        await cantonDB.createCanton("Valais");
        var canton = await models.Canton.findOne({id: 1 });
        assert.equal(canton.name, "Valais");
    });
});

/*
* Unit Test - Town Creation
*/
describe('CREATE TOWN DATABASE', () => {
   it('SHOULD CREATE A TOWN WITH THE CANTON', async () => {
       await townDB.createTown("Sion", 1);
       var town = await models.Town.findOne({id: 1 });
       assert.equal(town.name, "Sion");
   });
});

/**
 * Unit Test - Role Creation
 */
describe('CREATE ROLE DATABASE', () => {
    it('SHOULD CREATE A ROLE', async () => {
        await roleDB.createRole("admin");
        var role = await models.Role.findOne({id: 1 });
        assert.equal(role.name, "admin");
    });
});


/**
 * Unit Test - User Creation
 */
describe('CREATE USER DATABASE', () => {
    it('SHOULD CREATE A USER WITH THE CORRECT DEPENDENCIES', async () => {
        await userDB.createUser("Firstname", "Lastname", "email@email.com", "+00 00 000 00 00", "Address", "Audi", "VS 000 000","Password", 1, 1);
        var user = await models.User.findOne({id: 1 });
        assert.equal(user.firstname, "Firstname");
        assert.equal(user.lastname, "Lastname");
        assert.equal(user.email, "email@email.com");
        assert.equal(user.phone, "+00 00 000 00 00");
        assert.equal(user.address, "Address");
        assert.equal(user.brand, "Audi");
        assert.equal(user.licensePlate, "VS 000 000");
        var correctPassword = await bcrypt.compare("Password", user.password_hash);
        assert.equal(true, correctPassword);
        assert.equal(user.idRole, 1);
        assert.equal(user.idTown, 1);
    });
});

/**
 * Unit Test - Spot Creation
 */
describe('CREATE SPOT DATABASE', () => {
    it('SHOULD CREATE A SPOT WITH THE CORRECT DEPENDENCIES', async () => {
        var sion = await townDB.getTownByName("Sion");
        await spotDB.createSpot('1', 46.292781, 7.536189, "Avenue de la gare 17", "description", 2.10, 1, sion.id);
        var spot = await models.Spot.findOne({id: 1 });
        assert.equal(spot.latitude, 46.292781);
        assert.equal(spot.longitude, 7.536189);
        assert.equal(spot.address, "Avenue de la gare 17");
        assert.equal(spot.description, "description");
        assert.equal(spot.price, 2.10);
        assert.equal(spot.idUser, 1);
        assert.equal(spot.idTown, sion.id);
        assert.equal(spot.number, "1");
    });
});

/**
 * Unit Test - Disponibility Creation
 */
describe('CREATE DISPONIBILITY DATABASE', () => {
    it('SHOULD CREATE A DISPONIBILITY WITH THE CORRECT DEPENDENCIES', async () => {
        await disponibilityDB.createDisponibility("2018-04-01","13:00:00","15:00:00", 1, 1);
        var dispo = await models.Disponibility.findOne({id: 1});
        assert.equal(dispo.date, "2018-04-01");
    });
});


/**
 * Unit Test - Disponibility Creation with occurency
 */
describe('CREATE DISPONIBILITY DATABASE WITH OCCURENCY', () => {
    it('SHOULD CREATE A DISPONIBILITY WITH OCCURENCY', async () => {
        await disponibilityDB.createDisponibility("2018-04-01","09:00:00","11:00:00", 1, 3);
        var cpt = 0;
        var date = new Date("2018-03-25");
        for(var i = 0 ; i<3 ; i++){
            await models.Disponibility.findOne({where:{
                date: date.setDate(date.getDate() + 7),
                startHour: "09:00:00",
                endHour: "11:00:00"
            }}).then((dispo) => {
                if(dispo != null){
                    cpt++;
                }
            });
        }
        assert.equal(3,cpt);
    });
});

/**
 * Unit Test - Insert Error Disponibility existing
 */
describe('DONT CREATE DISPONIBILITY DATABASE WITH OCCURENCY', () => {
    it('SHOULD NOT CREATE A DISPONIBILITY WITH OCCURENCY', async () => {
        try
        {
            await disponibilityDB.createDisponibility("2018-04-01","13:00:00","15:00:00", 1, 1);
        } catch(error){
            assert.equal("DISPONIBILITY ALREADY EXISTS", error);
        }
    });
});