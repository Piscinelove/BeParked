const assert = require('assert');
const models = require("../models");
const townDB = require("../db/town_db");
const cantonDB = require("../db/canton_db");
const userDB = require("../db/user_db");
const roleDB = require("../db/role_db");
const spotDB = require('../db/spot_db');

/**
 * Unit Test - Select all spots
 */

describe('SELECT ALL SPOTS FROM DATABASE', () => {
    it('SHOULD RETURN A LENGTH OF 1', async () => {
        var spots = await spotDB.getAllSpots();
        assert.equal(1, spots.length);
    });
});

/**
 * Unit Test - Select all spots (empty array)
 */
describe('SELECT ALL SPOTS FROM DATABASE', () => {
    it('SHOULD RETURN THE ERROR : NO SPOTS FOUND', async () => {
        await models.Spot.destroy({where:{id:1}});
        try{
            await spotDB.getAllSpots();
        }catch(error){
            assert.equal("NO SPOTS FOUND", error);
        }
    });
});
