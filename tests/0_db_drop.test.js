const assert = require('assert');
const models = require("../models");

/**
 * drop everything from the db
 */
describe('DROP THE DB', () => {
    beforeEach((done) =>
    {
        models.sequelize.sync({ force: true }) // drops table and re-creates it
            .then(async() =>
            {
                done();
            })
            .catch((error) => {
                done(error);
            });
    });
    it('SHOULD RETURN TRUE',() => {
        assert.equal(true, true);
    });
});
