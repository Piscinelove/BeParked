const assert = require('assert');
const models = require("../models");

/**
 * close the connection with the database
 */
describe('CLOSE DB SESSION', () => {
    it('SHOULD RETURN TRUE',() => {
        assert.equal(true, true);
        models.sequelize.close();
    });
});