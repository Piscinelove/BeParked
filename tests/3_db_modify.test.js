const assert = require('assert');
const models = require("../models");
const townDB = require("../db/town_db");
const cantonDB = require("../db/canton_db");
const userDB = require("../db/user_db");
const roleDB = require("../db/role_db");
const spotDB = require('../db/spot_db');
const dispoDB = require('../db/disponibility_db');

/**
 * Unit Test - Update a spot
 */



describe('UPDATE SPOT IN THE DATABASE', () => {
    it('SHOULD RETURN TRUE',async () => {
        await spotDB.createSpot("200",100.99,100.90,"rue de la gare","proche des arbres",10.10,1,1);
        await spotDB.updateSpot(2, "100",99.99,90.90,"Avenue de HES","Place proche des ecoles",20.00,1);
        var spot = await models.Spot.findOne({id: 2 });
        assert.equal(spot.number, "100" );
        assert.equal(spot.latitude, 99.99 );
        assert.equal(spot.longitude,90.90 );
        assert.equal(spot.address, "Avenue de HES" );
        assert.equal(spot.description, "Place proche des ecoles" );
        assert.equal(spot.price, 20.00 );
        assert.equal(spot.idTown, 1 );
    });
});


/**
 * Unit Test - Update a user
 */

describe('UPDATE USER IN THE DATABASE', () => {
    it('SHOULD RETURN TRUE',async () => {
        await userDB.createUser("Lazic","Aleksandar","lazic@","0213","rue de la gare","Audi", "VS000000","123",1,1);
        await userDB.updateUser(1, "Kevin", "Carneiro", "0765012034", "Avenue Du Crochetan", "Audi", "VS000000",1);
        var user = await models.User.findOne({id:2});
        assert.equal(user.firstname, "Kevin" );
        assert.equal(user.lastname, "Carneiro");
        assert.equal(user.email, "email@email.com");
        assert.equal(user.phone, "0765012034");
        assert.equal(user.address, "Avenue Du Crochetan");
        assert.equal(user.brand, "Audi");
        assert.equal(user.licensePlate, "VS000000");
        assert.equal(user.idTown, 1);
    });
});


/**
 * Unit Test - Update a dispo
 */

describe('UPDATE DISPO IN THE DATABASE', () => {
    it('SHOULD RETURN TRUE',async () => {
        await dispoDB.createDisponibility("2018-04-01","13:00:00","15:00:00", 2, 1);
        await dispoDB.updateDispo(5,"1995-01-01","19:00:00","22:00:00");
        var dispo = await models.Disponibility.findOne({id:5});
        assert.equal(dispo.id, 5);
        assert.equal(dispo.date, "1995-01-01" );
        assert.equal(dispo.startHour, "19:00:00" );
        assert.equal(dispo.endHour,"22:00:00" );


    });
});