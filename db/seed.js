const models = require("../models");
const townDB = require("../db/town_db");
const cantonDB = require("../db/canton_db");
const userDB = require("../db/user_db");
const roleDB = require("../db/role_db");
const spotDB = require('../db/spot_db');
const dispoDB = require('../db/disponibility_db');
const bookDB = require('../db/book_db');
const settingDB = require('../db/setting_db');
const placesManagement = require('../modules/places_management');


function seed() {
    return new Promise((resolve, reject) => {
        models.sequelize.sync({ force: true }).then(async () => {
            await placesManagement.createTownsAndCantons();
            await roleDB.createRole("admin");
            await roleDB.createRole("user");
            var sierre = await townDB.getTownByName("Sierre");
            var chloe = await userDB.createUser("Chloé", "Bron", "chloe.viatte@students.hevs.ch", "077 777 77 77", "Avenue de la gare 12", "", "", "admin", 1, sierre.id);
            var johnDoe = await userDB.createUser("John", "Doe", "john.doe@gmail.com", "078 654 32 12", "Avenue de la gare 13", "Audi", "VS 345 432", "pass123", 2, sierre.id);
            var johnSmith = await userDB.createUser("John", "Smith", "john.smith@gmail.com", "078 654 32 12", "Avenue de la gare 14", "Audi", "VS 345 432", "pass123", 2, sierre.id);

            //create the first spot
            var spot1 = await spotDB.createSpot("1", "46.2921805", "7.532347500000014", "Place de la Gare", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam elit nulla, tempor ultrices erat vulputate et. Curabitur efficitur ullamcorper justo, sit amet vehicula purus laoreet sit amet.", 1.5, johnDoe.id, sierre.id);
            await spotDB.updateSpotPicture(spot1.id, "default.jpg")
            await dispoDB.createDisponibility("2018-04-26", "08:00:00", "16:00:00", spot1.id, 1);
            await dispoDB.createDisponibility("2018-04-27", "08:00:00", "16:00:00", spot1.id, 1);
            await dispoDB.createDisponibility("2018-04-28", "08:00:00", "16:00:00", spot1.id, 1);
            await dispoDB.createDisponibility("2018-04-29", "08:00:00", "16:00:00", spot1.id, 1);
            await dispoDB.createDisponibility("2018-04-30", "08:00:00", "16:00:00", spot1.id, 1);
            await dispoDB.createDisponibility("2018-05-01", "08:00:00", "16:00:00", spot1.id, 1);
            await bookDB.createBook("13:00:00", "15:30:00", 3.75, 3, 1, 1);
            await bookDB.createBook("13:00:00", "15:30:00", 3.75, 3, 1, 2);

            //create the second spot with no disponibilities and bookings
            var spot2 = await spotDB.createSpot("2", "46.292784", "7.536189", "Rue de la Plaine 2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam elit nulla, tempor ultrices erat vulputate et. Curabitur efficitur ullamcorper justo, sit amet vehicula purus laoreet sit amet.", 2.15, johnDoe.id, sierre.id);
            await spotDB.updateSpotPicture(spot2.id, "default2.jpg")

            //create the third spot
            var spot3 = await spotDB.createSpot("3", "46.2935835", "7.534757500000069", "Rue du Bourg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam elit nulla, tempor ultrices erat vulputate et. Curabitur efficitur ullamcorper justo, sit amet vehicula purus laoreet sit amet.", 3, johnDoe.id, sierre.id);
            await spotDB.updateSpotPicture(spot3.id, "default3.jpg")
            var dispoX = await dispoDB.createDisponibility("2018-04-26", "08:00:00", "12:00:00", spot3.id, 1);
            await dispoDB.createDisponibility("2018-04-26", "14:00:00", "18:00:00", spot3.id, 1);
            await dispoDB.createDisponibility("2018-04-27", "08:00:00", "12:00:00", spot3.id, 1);
            await dispoDB.createDisponibility("2018-04-27", "14:00:00", "18:00:00", spot3.id, 1);
            await dispoDB.createDisponibility("2018-04-28", "08:00:00", "12:00:00", spot3.id, 1);
            await dispoDB.createDisponibility("2018-04-28", "14:00:00", "18:00:00", spot3.id, 1);
            await bookDB.createBook("09:00:00", "10:00:00", 3, johnSmith.id, spot3.id, dispoX.id);
            await bookDB.createBook("10:30:00", "11:30:00", 3,johnSmith.id, spot3.id, dispoX.id);

            //create the fourth spot
            var spot4 = await spotDB.createSpot("4", "46.2937637", "7.535230100000035", "Avenue Max Huber 4", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam elit nulla, tempor ultrices erat vulputate et. Curabitur efficitur ullamcorper justo, sit amet vehicula purus laoreet sit amet.", 1.5, johnDoe.id, sierre.id);
            await spotDB.updateSpotPicture(spot4.id, "default4.jpg")
            await dispoDB.createDisponibility("2018-04-26", "08:00:00", "12:00:00", spot4.id, 1);
            await dispoDB.createDisponibility("2018-04-27", "08:00:00", "12:00:00", spot4.id, 1);
            await dispoDB.createDisponibility("2018-04-28", "08:00:00", "12:00:00", spot4.id, 1);

            //create the fifth spot
            var spot5 = await spotDB.createSpot("5", "46.2922291", "7.529140900000016", "Route des Lacustres 3", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam elit nulla, tempor ultrices erat vulputate et. Curabitur efficitur ullamcorper justo, sit amet vehicula purus laoreet sit amet.", 2, johnDoe.id, sierre.id);
            await spotDB.updateSpotPicture(spot5.id, "default5.jpg")

            await settingDB.createAbout("<p>Vous êtes propriétaire ou locataire d’une place de parc et cherchez à la rentabiliser ? Notre plateforme est faite pour vous !</p>\n" +
                "<p>Comme illustré dans la vidéo, notre plateforme propose aux propriétaires/locataires de places de parc de rentabiliser ces dernières en les proposant à la location lorsque celles-ci ne sont pas utilisées en permanence.</p>\n" +
                "<p>Nous collaborons avec les étudiants de la HES-SO pour mettre en place une phase de test à Sierre. Pour cela, nous recherchons des détenteurs de places acceptant de mettre à disposition leur place de parc lorsque celle-ci n’est pas utilisée. Nous sommes\n" +
                "    également à la recherche d'automobilistes qui désirent participer au projet-pilote. Les personnes mettant à disposition leur place de parc recevront une rémunération et les locataires disposeront des places à un prix préférentiel de 40 centimes par\n" +
                "    heure.</p>\n" +
                "<p>Une séance d’information sera organisée le 15 mai 2018 à 19h00 à l’Aula de la HES-SO de Sierre. Si vous êtes intéressé par le projet, merci de remplir le formulaire suivant :</p><a href=\"https://bit.ly/2GOyn7f\" target=\"_blank\">Formulaire d'inscription</a>\n" +
                "<p>Si vous désirez d’ores et déjà proposer votre place à la location, vous pouvez le faire en vous inscrivant<a href=\"/login\">  ici</a><span>  et en entrant les coordonnées de votre place.</span>\n" +
                "    <p>N'hésitez pas à nous contacter !</p>\n" +
                "</p>");
            await settingDB.createCGV("<h3>Conditions générales de BeParked</h3><i>Conditions d’utilisation</i> <p>En vous inscrivant sur la plateforme BeParked, vous vous engagez à respecter les termes et conditions énoncées ci-dessous. Si vous êtes en désaccord avec les termes et conditions, veuillez vous désinscrire et annuler votre compte. Nous nous réservons     le droit de modifier les conditions d’utilisation sans préavis. Les nouvelles conditions entreront en vigueur dès qu’elles seront postées sur la plateforme BeParked. Nous vous recommandons donc de consulter de manière régulière les conditions générales.</p><i>Description du service</i> <p>BeParked est une plateforme en ligne gratuite destinée aux personnes majeures, ouverte aux bénéficiaires de places de parc et aux automobilistes. Ce service met en lien les bénéficiaires qui peuvent proposer de sous-louer leurs places de parc lorsqu’elles     sont inoccupées et les automobilistes qui cherchent une place pour un laps de temps prédéfini. Pour profiter de la plateforme, l’utilisateur doit bénéficier de sa propre connexion internet et d’un outil adapté à son utilisation.</p><i>Droit de propriété</i> <p>BeParked est propriétaire de l’intégralité des données présentes sur le site, que ce soit les textes, les photos, les images, etc. Le service est protégé par les lois sur le droit de la propriété intellectuelle conformément aux lois en vigueur. Toutes     reproductions, totales ou même partielles, constitueraient des actes de contrefaçons. Il est donc interdit de copier, reproduire, vendre ou exploiter à des fins commerciales, toute partie du service (sauf accord écrit de BeParked).</p><i>Responsabilité de BeParked</i> <p>BeParked décline toute responsabilité en cas de dégât ou de vol ayant eu lieu lors d’un stationnement dans le cadre de la sous-location. De plus, BeParked décline toute responsabilité en cas d’utilisation abusive de la place de parc de la part d’un automobiliste.     Cependant, selon l’article 926 du CC, « le possesseur a le droit de repousser par la force tout acte d'usurpation ou de trouble ». Le terme « acte d’usurpation » englobe également le stationnement d’une voiture sans autorisation à un emplacement non     public. Le bénéficiaire peut donc se réserver le droit d’appeler la police, tout en étant conscient de la procédure qui peut s’ensuivre. Le propriétaire doit aussi savoir qu’à partir du moment où il met une plage horaire à disposition, il ne peut     pas revenir sur sa décision si elle a été réservée par un automobiliste. Toutefois, BeParked se réserve le droit d’instaurer un système de sanctions à l’interne de la plateforme pour les utilisateurs abusifs.</p><i>Votre responsabilité légale</i> <p>Si, en tant que bénéficiaire de place de parc, vous louez votre place à une régie immobilière, vous devez être conscient des lois qui régissent la sous-location de places de parc. En principe, vous n’avez pas le droit se faire des bénéfices sur votre     place. De plus, vous avez l’obligation d’annoncer à votre régie que vous mettez votre place en sous-location. Il peut vous être demandé par votre régie de fournir un décompte de ce que vous avez gagné grâce à BeParked. À votre inscription, nous vous     demandons donc une copie de votre contrat de bail, ainsi qu’une photographie de votre place de parc. BeParked décline toute responsabilité si vous ne respectez pas la loi concernant la sous-location de votre place de parc. Toutefois, BeParked propose     de garder vos bénéfices sur votre compte, afin que vous en bénéficiiez lors de l’utilisation de la plateforme en tant qu’automobiliste.</p><i>Votre compte</i> <p>Tous les membres enregistrés doivent utiliser un mot de passe et un nom d'utilisateur. Les membres sont entièrement responsables de toutes les activités qui se produisent sous leur compte. Le membre s'engage à ne pas faire un usage commercial du service     sans le consentement écrit de BeParked. Ne seront pas acceptées sur ce site, toutes annonces n’étant pas en rapport avec la location de place de parking, offensantes pour la communauté, dont le contenu favorise le racisme, le sectarisme, la haine     ou la violence physique de toute nature contre un groupe ou une personne, préjudiciables aux mineurs, à caractère pornographique, menaçants, obscènes, diffamatoires ou calomnieux, susceptibles de porter atteinte au respect et à la dignité humaine.     Cette liste n'est pas exhaustive, mais plutôt un guide pour vous aider à voir ce qui est approprié sur cette plateforme.</p><i>Résiliation du compte</i> <p>Vous reconnaissez à BeParked le droit de mettre fin à tout ou partie du droit d'accès correspondant à votre compte et votre mot de passe, voire de supprimer votre compte et mot de passe, ainsi que le droit de retirer ou de déplacer toute annonce et ce     pour tout motif notamment en raison de SPAM, flood ou si BeParked a de bonnes raisons de croire que vous avez violé ou agi en contradiction avec la loi ou l'esprit des présentes conditions d'utilisation.</p><i>Modification des annonces</i> <p>BeParked se réserve le droit de supprimer tout contenu ne respectant pas ses termes et conditions. En tant qu'utilisateur, vous reconnaissez également à BeParked le droit d’archiver les annonces, et de divulguer ces derniers dans le cadre d’une procédure     judiciaire visant à faire respecter les lois en vigueur, les conditions d’utilisation du service, les droits et intérêts de BeParked.</p><i>Respect des lois</i> <p>Chaque membre assume la connaissance des lois applicables et en est responsable du respect de ces lois. Le membre ne peut en aucun cas utiliser ce service d'une manière qui viole les lois d’état ou fédérales. Le membre s'engage également à ne transmettre     aucun matériel qui encourage une conduite qui pourrait constituer une infraction pénale, donner lieu à une responsabilité civile, violer toute loi cantonale, fédérale, nationale ou internationale.</p>");
            resolve();
        });
    })
}

seed();

exports.seed = seed;
