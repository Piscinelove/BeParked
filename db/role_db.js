var models = require('../models');

/**
 * CREATE ROLE IN DATABASE
 * @param {string} name 
 */
function createRole(name){
    return new Promise((resolve, reject) => {
        models.Role.create({
            name: name
        }).then((role) => {
            console.log("ROLE #"+role.id+" CREATED");
            resolve(role);
        });
    })
}

exports.createRole = createRole;