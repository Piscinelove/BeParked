var models = require('../models');
var Op = require('sequelize').Op;


/**
 * CREATE TOWN IN DATABASE
 * @param {string} name
 */
function createTown(name, idCanton){
    return new Promise((resolve, reject) => {
        models.Town.findOrCreate({
            where:{name: name},
            defaults:{idCanton: idCanton}
        }).spread((town,inserted) => {
            console.log("TOWN #"+town.id+" CREATED");
            resolve(town);
        });
    })
}

/**
 * FIND TOWN BY NAME
 * @param {string} name
 */
function getTownByName(name){
    return new Promise((resolve, reject) => {
        models.Town.findOne({
            where:{name: name}
        }).then((town) => {
            console.log("TOWN #"+town.id+" FOUNDED");
            resolve(town);
        });
    })
}

/**
 * GET ALL TOWNS
 * @param input
 * @returns {Promise<any>}
 */
function getAllTowns(input){
    return new Promise((resolve, reject) => {
        models.Town.findAll({
            where:{
                name:{
                    [Op.like]: '%' + input + '%'
                }
            }
        }).then((towns) => {resolve(towns)});
    })
}

/**
 * UPDATE A TOWN USING IT'S ID
 * @param id
 * @param name
 * @returns {Promise<any>}
 */
function updateTownById(id, name)
{
    return Promise.resolve(
        models.Town.update(
            {
                name: name

            },
            {
                where: {id: id}
            }
        )
    )
}

exports.updateTownById = updateTownById;
exports.getAllTowns = getAllTowns;
exports.createTown = createTown;
exports.getTownByName = getTownByName;