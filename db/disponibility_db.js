var models = require('../models');
var bookDB = require('./book_db');
var Op = require('sequelize').Op;


/**
 * CREATE A NEW DISPONIBILITY IF IT DOESN'T OVERLAP WITH ANOTHER DISPONIBILITY
 * @param date
 * @param startHour2
 * @param endHour2
 * @param idSpot
 * @param occurence
 * @returns {Promise<any>}
 */
function createDisponibility(date, startHour2, endHour2, idSpot, occurence){
    return new Promise((resolve, reject) => {
        var newDate = new Date(date);
        newDate.setDate(newDate.getDate() - 7);
        for(var i = 0 ; i<occurence ; i++){
            models.Disponibility.findOrCreate
            ({
                where:{
                    date: (newDate.setDate(newDate.getDate() + 7)),
                    [Op.and]: [{endHour:{gte: startHour2}}, {startHour:{lte: endHour2}}],
                    idSpot:idSpot
                },
                defaults:
                    {
                        startHour: startHour2,
                        endHour: endHour2,
                        idSpot: idSpot
                    }
            }).spread(async (disponibility, inserted) =>{
                if(inserted)
                {
                    console.log("DISPONIBILITY #"+disponibility.id+" HAS BEEN CREATED");
                    resolve(disponibility);
                }
                else
                {
                    if(disponibility.startHour > startHour2 && disponibility.endHour < endHour2)
                    {
                        var bookings = await disponibility.getBookings();
                        await extendDisponibility(disponibility.id);
                        var newDisponibility = await createDisponibility(date, startHour2, endHour2, idSpot, occurence);
                        for(let i = 0; i < bookings.length; i++)
                        {
                            await bookDB.updateBookingDisponibility(bookings[i].id, newDisponibility.id);
                        }

                        resolve(newDisponibility);
                    }
                    //if(disponibility.endHour === startHour2)
                    else if(disponibility.endHour < endHour2)
                    {
                        var bookings = await disponibility.getBookings();
                        await extendDisponibility(disponibility.id);
                        var newDisponibility = await createDisponibility(date, disponibility.startHour, endHour2, idSpot, occurence);
                        for(let i = 0; i < bookings.length; i++)
                        {
                            await bookDB.updateBookingDisponibility(bookings[i].id, newDisponibility.id);
                        }

                        resolve(newDisponibility);



                    }
                    //else if(disponibility.startHour === endHour2)
                    else if(disponibility.startHour > startHour2)
                    {
                        var bookings = await disponibility.getBookings();
                        await extendDisponibility(disponibility.id);
                        var newDisponibility = await createDisponibility(date, startHour2, disponibility.endHour, idSpot, occurence);
                        for(let i = 0; i < bookings.length; i++)
                        {
                            await bookDB.updateBookingDisponibility(bookings[i].id, newDisponibility.id);
                        }

                        resolve(newDisponibility);
                    }
                    else
                        reject("DISPONIBILITY ALREADY EXISTS");
                }
            })
        }
    })
}

/**
 * RETRIEVE A LIST OF DISPONIBILITIES BY DATE
 * @param date
 * @returns {Promise<any>}
 */
function getAllDisponibilitiesByDate(date){
    return new Promise((resolve, reject) => {
        models.Disponibility.findAll({
            where:{
                date: date
            }
        }).then((disponibilities) => {
            if(disponibilities.length > 0){
                resolve(disponibilities);
            } else {
                reject("NO DISPONIBILITIES FOUND");
            }
        })
    })
}

/**
 * EXTEND ONE DISPONIBILITY
 * @param id
 * @returns {Promise<any>}
 */
function extendDisponibility(id)
{
    return Promise.resolve(
        models.Disponibility.destroy
        ({
            where:{id:id}
        })
    )
}

/**
 * UPDATE A DISPONIBILITY
 * @param id
 * @param date
 * @param startHour
 * @param endHour
 * @returns {Promise<any>}
 */
function updateDispo(id, date,startHour,endHour)
{
    return Promise.resolve(
        models.Disponibility.update(
            {
                date: date,
                startHour: startHour,
                endHour: endHour
            },
            {
                where: {id: id}
            }
        )
    )
}

/**
 * DELETE A DISPONIBILITY
 * @param id
 * @returns {Promise<any>}
 */
function deleteDispo(id) {
    return new Promise((resolve, reject) => {
        models.Disponibility.destroy
        ({
            where: {
                id: id
            }
        }).then(() => {
            resolve("DISPONIBILITY DELETED");
        });

    })
}

/**
 * DELETE A DISPONIBILITY USING ITS ID SPOT
 * @param idSpot
 * @returns {Promise<any>}
 */
function deleteDispoByIdSpot(idSpot) {
    return new Promise((resolve, reject) => {
        models.Disponibility.destroy
        ({
            where: {
                idSpot: idSpot
            }
        }).then(() => {
            resolve("DISPONIBILITY DELETED");
        });

    })
}

exports.deleteDispoByIdSpot = deleteDispoByIdSpot;
exports.deleteDispo = deleteDispo;
exports.updateDispo = updateDispo;
exports.createDisponibility = createDisponibility;
exports.getAllDisponibilitiesByDate = getAllDisponibilitiesByDate;