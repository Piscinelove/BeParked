var models = require('../models');
var sequelize = require('sequelize')


/**
 * CREATE A SPOT
 * @param {string} number
 * @param {*double} latitude
 * @param {*double} longitude
 * @param {*string} address
 * @param {*string} description
 * @param {*string} picture
 * @param {*decimal} price
 * @param {*int} idUser
 * @param {*int} idTown
 */
function createSpot(number, latitude, longitude, address, description, price, idUser, idTown)
{
    return new Promise((resolve, reject) => {
        models.Spot.create({
            number: number,
            latitude: latitude,
            longitude: longitude,
            address: address,
            description: description,
            price: price,
            idUser: idUser,
            idTown: idTown,
        }).then((spot) => {
            resolve(spot);
        });
    })
}

/**
 * GET SPOT BY ID
 * @param id
 * @returns {Promise<any>}
 */
function getSpotById(id) {
    return new Promise((resolve, reject) => {
        models.Spot.findOne({
            where: {
                id: id
            },
            include:
                [
                    { model: models.Town },
                    { model: models.Disponibility },
                    {
                        model: models.Booking,
                        required: false,
                        include:
                            [
                                { model: models.Disponibility, attributes: ['date'] },
                                {
                                    model: models.User
                                }
                            ]
                    }
                ]
        }).then((spot) => {
            console.log(spot);
            resolve(spot);
        });
    })
}

/**
 * RETRIEVE ALL SPOTS STORED IN THE DATABASE
 * @returns {Promise<Spots[]>}
 */
function getAllSpots() {
    return new Promise((resolve, reject) => {
        models.Spot.findAll().then((spots) => {
            if (spots.length > 0) {
                resolve(spots);
            } else {
                reject("NO SPOTS FOUND");
            }
        });
    })
}

/**
 * GET ALL AVAILABLE SPOTS
 * date : YYYY-MM-DD
 * startHour : "09:00:00"
 * startHour : "10:00:00"
 * @param {string} date
 * @param {*string} startHour
 * @param {*string} endHour
 */
function getAllAvailableSpots(date, startHour, endHour, idUser) {
    return new Promise((resolve, reject) => {
        models.Spot.findAll(
            {
                where: {
                    idUser: {$not: idUser}
                },
                include:
                    [
                        { model: models.Town },
                        {
                            model: models.Disponibility,
                            include:
                                [
                                    {
                                        model: models.Booking,
                                    }
                                ]
                        }
                    ]
            }).then((spots) => {

                if (spots.length > 0) {
                    resolve(spots);
                } else {
                    reject("No spots available with those disponibilities");
                }
            });
    })
}

/**
 * RETRIEVE A SPOT PICTURE
 * @param id
 * @returns {Promise<any>}
 */
function getSpotPicture(id) {
    return new Promise((resolve, reject) => {
        models.Spot.findOne({
            where: {
                id: id
            },
            attributes: ['picture']
        }).then((spot) => {
            resolve(spot.picture);
        });
    })
}
/**
 * DELETE SPOT IN DATABASE
 * @param id
 * @returns {Promise<any>}
 */
function deleteSpot(id) {
    return Promise.resolve(
        models.Spot.destroy(
            {
                where: { id: id }
            }
        )
    )
}

/**
 * UPDATE A SPOT
 * @param id
 * @param number
 * @param latitude
 * @param longitude
 * @param address
 * @param description
 * @param price
 * @param idTown
 * @returns {Promise<any>}
 */
function updateSpot(id, number, latitude, longitude, address, description, price, idTown) {
    return Promise.resolve(
        models.Spot.update(
            {
                number: number,
                latitude: latitude,
                longitude: longitude,
                address: address,
                description: description,
                price: price,
                idTown: idTown
            },
            {
                where: { id: id }
            }
        )
    )
}

/**
 * UPDATE A SPOT PICTURE
 * @param idSpot
 * @param picture
 * @returns {Promise<any>}
 */
function updateSpotPicture(idSpot, picture) {
    return new Promise((resolve, reject) => {
        models.Spot.update(
            {
                picture: picture,
            },
            {
                where: { id: idSpot }
            }
        ).then((spot) => {
            resolve(spot);
        });
    })
}

exports.getSpotPicture = getSpotPicture;
exports.getSpotById = getSpotById;
exports.updateSpot = updateSpot;
exports.createSpot = createSpot;
exports.getAllSpots = getAllSpots;
exports.getAllAvailableSpots = getAllAvailableSpots;
exports.deleteSpot = deleteSpot;
exports.updateSpotPicture = updateSpotPicture;
