const models = require('../models');
/*
create cgv with the text in parameter
 */
function createCGV(text) {
    return new Promise((resolve, reject) => {
        models.Setting.create({
            key: "cgv",
            text: text
        }).then(() => {
            resolve();
        });
    })
}

/*
create an About
 */
function createAbout(text) {
    return new Promise((resolve, reject) => {
        models.Setting.create({
            key: "about",
            text: text
        }).then(() => {
            resolve();
        });
    })
}
/*
update the cgv with the text
 */
function updateCGV(text) {
    return new Promise((resolve, reject) => {
        models.Setting.update(
            {
                text: text
            },
            {
                where: {key: "cgv"}
            }
        ).then((text)=>{
            resolve(text);
        });
    })
}

/*
update the about with the text
 */
function updateAbout(text) {
    return new Promise((resolve, reject) => {
        models.Setting.update(
            {
                text: text
            },
            {
                where: {key: "about"}
            }
        ).then((text)=>{
            resolve(text);
        });
    })
}

function getAbout() {
    return new Promise((resolve, reject) => {
        models.Setting.findOne({
            where: {key: "about"},
        }).then((about) => {
            resolve(about);
        });
    })
}

function getCGV() {
    return new Promise((resolve, reject) => {
        models.Setting.findOne({
            where: {key: "cgv"},
        }).then((about) => {
            resolve(about);
        });
    })
}

exports.createCGV = createCGV;
exports.createAbout = createAbout;
exports.updateCGV = updateCGV;
exports.updateAbout = updateAbout;
exports.getAbout = getAbout;
exports.getCGV = getCGV;