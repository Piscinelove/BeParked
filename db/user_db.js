const models = require('../models');

/**
 * CREATE USER IN DATABASE METHODE
 * @param firstname
 * @param lastname
 * @param email
 * @param phone
 * @param address
 * @param password
 * @param idRole
 * @param idTown
 * @returns {Promise<User>}
 */
function createUser(firstname, lastname, email, phone, address, brand, licensePlate, password, idRole, idTown) {
    console.log("DANS CREATE USER DB");
    return new Promise((resolve, reject) => {
        models.User.findOrCreate
            ({
                where:
                    {
                        email: email
                    },
                defaults:
                    {
                        firstname: firstname,
                        lastname: lastname,
                        phone: phone,
                        address: address,
                        password: password,
                        sold: 0,
                        brand: brand,
                        licensePlate: licensePlate,
                        idRole: idRole,
                        idTown: idTown
                    }
            }).spread((user, inserted) => {
                if (inserted) {
                    console.log(inserted)
                    console.log("USER #" + user.id + " HAS BEEN CREATED");
                    resolve(user);
                }
                else {
                    console.log(inserted);
                    reject("USER IDENTIFIED BY " + email + " ALREADY EXISTS");
                }
            })
    })
}

/**
 * FIND USER BY EMAIL IN THE DATABSE
 * @param email
 * @returns {Promise<User>}
 */
function getUserByEmail(email) {
    return new Promise((resolve, reject) => {
        models.User.findOne
            ({
                where: { email: email }
            }).then((user) => {
                if (user != null) {
                    console.log("USER #" + user.id + " FOUND");
                    resolve(user);
                }
                else
                    reject("NO USER FOUND IDENTIFIED BY " + email);

            })
    })
}

/**
 * GET A USER BY ID
 * @param id
 * @returns {Promise<any>}
 */
function getUserById(id) {
    return new Promise((resolve, reject) => {
        models.User.findOne({
            where: { id: id },
            include:
                [
                    { model: models.Town },
                    { model: models.Role },
                    {
                        model: models.Booking,
                        model: models.Booking,
                        include:
                            [
                                { model: models.Disponibility, attributes: ['date'] }
                            ]
                    },
                    {

                        model: models.Spot,
                        include:
                            [
                                { model: models.Town },
                                { model: models.Disponibility },
                                {
                                    model: models.Booking,
                                    include:
                                        [
                                            { model: models.Disponibility, attributes: ['date'] }
                                        ]
                                }
                            ]
                    }
                ]
        }).then((user) => {
            resolve(user);
        });
    })
}

/**
 * GET ALL USERS EXEPT CURRENT USER
 * @param idUserAdmin
 * @returns {Promise<any>}
 */
function getAllUsersExceptCurrent(idUserAdmin) {
    return new Promise((resolve, reject) => {
        models.User.findAll({
            where: { id: { $not: idUserAdmin } },
            include:
                [
                    { model: models.Town },
                    { model: models.Role },
                    {

                        model: models.Spot,
                        include:
                            [
                                { model: models.Town },
                                { model: models.Disponibility },
                                { model: models.Booking }
                            ]
                    }
                ]
        }).then((users) => {
            resolve(users);
        });
    })
}

/**
 * UPDATE USER AND PASSWORD
 * @param id
 * @param firstname
 * @param lastname
 * @param phone
 * @param address
 * @param brand
 * @param licensePlate
 * @param password
 * @param idTown
 * @returns {Promise<any>}
 */
function updateUserAndPassword(id, firstname, lastname, phone, address, brand, licensePlate, password, idTown) {
    return Promise.resolve(
        models.User.update(
            {
                firstname: firstname,
                lastname: lastname,
                phone: phone,
                address: address,
                brand: brand,
                licensePlate: licensePlate,
                password: password,
                idTown: idTown

            },
            {
                where: { id: id }
            }
        )
    )
}

/**
 * UPDATE USER WITHOUT PASSWORD
 * @param id
 * @param firstname
 * @param lastname
 * @param phone
 * @param address
 * @param brand
 * @param licensePlate
 * @param idTown
 * @returns {Promise<any>}
 */
function updateUser(id, firstname, lastname, phone, address, brand, licensePlate, idTown) {
    return Promise.resolve(
        models.User.update(
            {
                firstname: firstname,
                lastname: lastname,
                phone: phone,
                address: address,
                brand: brand,
                licensePlate: licensePlate,
                idTown: idTown

            },
            {
                where: { id: id }
            }
        )
    )
}

/**
 * DELETE USER
 * @param id
 * @returns {Promise<any>}
 */
function deleteUser(id) {
    return new Promise((resolve, reject) => {
        models.User.destroy
            ({
                where: {
                    id: id
                }
            }).then(() => {
                resolve("USER DELETED");
            });

    })
}



module.exports.deleteUser = deleteUser;
module.exports.updateUser = updateUser;
module.exports.updateUserAndPassword = updateUserAndPassword;
module.exports.createUser = createUser;
module.exports.getUserByEmail = getUserByEmail;
module.exports.getUserById = getUserById;
module.exports.getAllUsersExceptCurrent = getAllUsersExceptCurrent;