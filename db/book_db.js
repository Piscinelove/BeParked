var models = require('../models');
var Op = require('sequelize').Op;

/**
 * CREATE A BOOK IN THE DATABASE
 * @param startHour
 * @param endHour
 * @param price
 * @param idUser
 * @param idSpot
 * @param idDisponibility
 * @returns {Promise<any>}
 */
function createBook(startHour, endHour, price, idUser, idSpot, idDisponibility) {
    return new Promise((resolve, reject) => {
        models.Booking.create({

            startHour: startHour,
            endHour: endHour,
            price: price,
            idUser: idUser,
            idSpot: idSpot,
            idDisponibility: idDisponibility

        }).then((book) => {
            console.log("BOOK #" + book.id + " CREATED");
            resolve(book);
        });
    })
}

/**
 * UPDATE BOOKING DISPONIBILITY IN DB
 * @param id
 * @param newIdDisponibility
 * @returns {Promise<any>}
 */
function updateBookingDisponibility(id, newIdDisponibility) {
    return Promise.resolve(
        models.Booking.update(
            {
                idDisponibility: newIdDisponibility,
            },
            {
                where: { id: id }
            }
        )
    )
}

/**
 * DELETE BOOKING IN DATABASE
 * @param id
 * @returns {Promise<any>}
 */
function deleteBooking(id) {
    return Promise.resolve(
        models.Booking.destroy(
            {
                where: { id: id }
            }
        )
    )
}

/**
 * GET ALL BOOKINGS USING ITS DISPONIBILITY
 * @param idDisponibility
 * @returns {Promise<any>}
 */
function getBookingsByDisponibility(idDisponibility) {
    return new Promise((resolve, reject) => {
        models.Booking.findAll({
            where: {
                idDisponibility: idDisponibility
            }
        }).then((bookings) => {
            if (bookings.length > 0) {
                reject("Impossible de supprimer cette disponibilité, des réservations ont été trouvées");
            } else {
                resolve("Success, no bookings for this disponibility")
            }
        });
    })
}

/**
 * GET ALL BOOKINGS RELATED TO ONE SPOT
 * @param idSpot
 * @returns {Promise<any>}
 */
function getBookingsBySpot(idSpot) {
    return new Promise((resolve, reject) => {
        models.Booking.findAll({
            where: {
                idSpot: idSpot,
            },
            include:[
                {
                    model: models.Disponibility,
                    where:{
                        date: getCurrentDate()
                    },
                    required: false,
                }
            ]
        }).then((bookings) => {
            if (bookings.length > 0) {
                reject("Impossible de supprimer la place de parc, des réservations ont été trouvées");
            } else {
                resolve("Success, no bookings for this disponibility")
            }
        });
    })
}


/**
 * RETRIEVE ALL BOOKINGS
 * @returns {Promise<any>}
 */
function getAllReservations() {
    return new Promise((resolve, reject) => {
        models.Booking.findAll({
            include:
                [
                    { model: models.User },
                    { model: models.Disponibility },
                    {
                        model: models.Spot,
                        include:
                            [
                                {
                                    model: models.User,
                                }
                            ]
                    }
                ]
        }).then((bookings) => {
            resolve(bookings);
        });
    })
}

/**
 * RETRIEVE ALL BOOKINGS BY USER ID
 * @param idUser
 * @returns {Promise<any>}
 */
function getAllReservationsByUser(idUser) {
    return new Promise((resolve, reject) => {
        models.Booking.findAll({
            where:{
                idUser: idUser
            },
            include:
                [
                    { model: models.User },
                    { model: models.Disponibility },
                    {
                        model: models.Spot,
                        include:
                            [
                                {
                                    model: models.User,
                                }
                            ]
                    }
                ]
        }).then((bookings) => {
            resolve(bookings);
        });
    })
}

/**
 * DELETE ALL BOOKINGS OF ONE USER
 * @param idUser
 * @returns {Promise<any>}
 */
function deleteBookingsOfUser(idUser){
    return Promise.resolve(
        models.Booking.destroy(
            {
                where: { idUser: idUser }
            }
        )
    )
}

/**
 * DELETE BOOKINGS OF ONE SPOT
 * @param idSpot
 * @returns {Promise<any>}
 */
function deleteBookingsOfSpot(idSpot){
    return Promise.resolve(
        models.Booking.destroy(
            {
                where: { idSpot: idSpot }
            }
        )
    )
}

/**
 * RETRIEVE THE CURRENT DATE
 * @returns {string|Date}
 */
function getCurrentDate() {
    // get the current date
    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth() + 1;
    var year = today.getFullYear();

    if (day < 10) {
        day = '0' + day
    }
    if (month < 10) {
        month = '0' + month
    }
    today = year + "-" + month + "-" + day;
    return today;
}

exports.getAllReservationsByUser = getAllReservationsByUser;
exports.getBookingsBySpot = getBookingsBySpot;
exports.getBookingsByDisponibility = getBookingsByDisponibility;
exports.deleteBooking = deleteBooking;
exports.createBook = createBook;
exports.updateBookingDisponibility = updateBookingDisponibility;
exports.getAllReservations = getAllReservations;
exports.deleteBookingsOfSpot = deleteBookingsOfSpot;
exports.deleteBookingsOfUser = deleteBookingsOfUser;