var models = require('../models');

/**
 * CREATE CANTON IN DATABASE
 * @param {string} name
 */
function createCanton(name){
    return new Promise((resolve, reject) => {
        models.Canton.findOrCreate({
            where: {name: name}
        }).spread((canton,inserted) => {
            console.log("CANTON #"+canton.id+" CREATED");
            console.log(canton.id+"djioa");
            resolve(canton);
        });
    })
}


exports.createCanton = createCanton;