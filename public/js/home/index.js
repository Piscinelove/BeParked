var socket = io();

$(document).ready(function () {
    $('.sidenav').sidenav();
    initMap();

});

socket.on('contact_form_result', function () {
    var toastHTML = '<span>Merci pour votre message !</span><button class="btn-flat toast-action"><i class="material-icons">check</i></button>';
    M.toast({ html: toastHTML });
    $('#form-contact')[0].reset();

})

/*
get the input values and send an email with it
 */
function contact() {
    if($('#form-contact').valid())
    {
        $name = $('#contact-name').val();
        $email = $('#contact-email').val();
        $subject = $('#contact-object').val();
        $message = $('#contact-description').val();

        socket.emit('contact_form', $name, $email, $subject, $message);
    }

}

function initMap() {
    var beparked = { lat: 46.3, lng: 7.5333 };
    var map = new google.maps.Map(document.getElementById('map-home'), {
        zoom: 15,
        center: beparked
    });
    var marker = new google.maps.Marker({
        position: beparked,
        map: map
    });
}



