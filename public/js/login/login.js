$(document).ready(function(){
    $('#login-signup').tabs();
    $('select').formSelect();
    $('.datepicker').datepicker({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        monthsFull: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        weekdaysShort: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
        today: "Aujourd'hui",
        clear: "Annuler",
        close: "OK",
        closeOnSelect: true ,// Close upon selecting a date,
        format: 'mm/dd/yyyy',
        min:true
    });
    $('.stepper').activateStepper({
    });
});

