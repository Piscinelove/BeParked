var $defaultOptions = {center: {lat: 46.292781, lng: 7.536189}, zoom: 13, mapTypeId: 'satellite'}


$(document).ready(function () {
    initMap();
    $('textarea#description-spot').characterCounter();
    $("#address-spot").geocomplete({details: "form#form-spot-add"});
});


function initMap() {
    //LOADING UI ELEMENTS
    $input = document.getElementById('spots-address-spot');
    $searchForm = document.getElementById('spots-form-search');
    $latitudeField = document.getElementById('latitude-field');
    $longitudeField = document.getElementById('longitude-field');

    //LOADING GOOGLE MAP FUNCTIONS
    $autocomplete = new google.maps.places.Autocomplete($input);
    $searchBox = new google.maps.places.SearchBox($input);
    //LOADING GOOGLE MAP
    $map = new google.maps.Map(document.getElementById('map-spot'), $defaultOptions);
    //TESTING IF GOOGLE MAP IS AVAILABLE IN THE BROWSER
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var $position = {lat: position.coords.latitude, lng: position.coords.longitude};
            $map.panTo($position);

            var marker = new google.maps.Marker(
                {
                    position: $position,
                    map: $map,
                    icon:
                        {
                            url: "/images/icons/location.svg",
                            scaledSize: new google.maps.Size(43, 43)
                        },
                    zoom: 10

                });
            marker.setAnimation(google.maps.Animation.DROP);

            closeLoading();

        }, function () {
            //GEOLOCALISATION ERROR USE DEFAULT LOCALISATION
            $map.panTo($defaultOptions.center);

            var marker = new google.maps.Marker(
                {
                    position: $defaultOptions.center,
                    map: $map,
                    icon:
                        {
                            url: "/images/icons/location.svg",
                            scaledSize: new google.maps.Size(43, 43)
                        },
                    animation: google.maps.Animation.DROP
                });

            closeLoading();

        }, {timeout: 10000});
    } else {
        //BROWSER DON'T SUPPORT GEOLOCALISATION
        handleLocationError(false, infoWindow, $map.getCenter());

    }

    // Add Listner to the map
    $map.addListener('bounds_changed', function () {
        $searchBox.setBounds($map.getBounds());
    });

    var markers = [];
    // Listen for the event on the search box
    $searchBox.addListener('places_changed', function () {
        var places = $searchBox.getPlaces();
        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            var marker = new google.maps.Marker({
                draggable: true,
                map: $map,
                icon:
                    {
                        url: "/images/icons/destination.svg",
                        scaledSize: new google.maps.Size(43, 43)
                    },
                title: place.name,
                position: place.geometry.location,
                zoom: 30
            });

            //update the fields longitude and latitude
            $latitudeField.value = place.geometry.location.lat();
            $longitudeField.value = place.geometry.location.lng();

            //add listner to the markers
            marker.addListener('drag', updateFieldsCoord);
            marker.addListener('dragend', updateFieldsCoord);

            markers.push(marker);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        $map.fitBounds(bounds);
    });


}

/*
Update the coordinates in the fields when we drag and drop the marker
 */
function updateFieldsCoord(event) {
    $latitudeField.value = event.latLng.lat();
    $longitudeField.value = event.latLng.lng();
}

/*
close loading page
 */
function closeLoading() {
    $('#spot-loading-overlay').hide();
}

