$(document).ready(function () {
    //DEFAULT MODE OF SPOT DETAILS
    defautltModeSpotDetails();
    $("#address-details-disable").geocomplete({details: "form#details-general-form"});

    //get current id
    var url = (window.location.href).split('=');
    $idSpot = url[url.length - 1];

    //get current Spot
    getCurrentSpot();

    $('.tooltipped').tooltip();
});

//----------------------------- SOCKET IO -----------------------------------------------------------------------

var socket = io();

/**
 * wait on server response for spot update result
 */
socket.on('update_spot_result', function (spot) {
    var toastHTML = '<span>Place de parc modifié avec succès</span>';
    M.toast({html: toastHTML});
    $('#details-tabs').load(' #details-tabs');
})

/**
 * wait on server response for add disponibility result
 */
socket.on('add_disponibility_spot_result', function (result) {
    console.log(result + "salut");
    var toastHTML = '<span>' + result + '</span>';
    M.toast({html: toastHTML});
    getCurrentSpot();
    $('#details-dispo-form')[0].reset();
})

/**
 * wait on server response for the current spot
 */
socket.on('get_spot_details_result', function (spot) {
    $spot = spot;
    renderCalendar();
})
/**
 * wait on server response for the deletion of booking
 */
socket.on('delete_booking_result', async (result) => {
    var toastHTML = '<span>' + result + '</span>';
    M.toast({html: toastHTML});
    $('#details-booking').load(' #details-booking');
})

/**
 * wait on server response for the deletion of disponibility
 */
socket.on('delete_disponibility_result', async (result) => {
    var toastHTML = '<span>' + result + '</span>';
    M.toast({html: toastHTML});
    getCurrentSpot();
})


//-----------------------------FIRST SECTION EDIT / SAVE MODE -----------------------------------------------------------------------

/**
 * DEFAULT MODE DISABLING ALL INPUTS
 * HIDING SAVE BUTTON SHOWING EDIT BUTTON
 */
function defautltModeSpotDetails() {
    $('#details-general-form :input:not(button)').prop("disabled", true);
    $("#save-details-info").hide();
    $("#edit-details-info").show();
}

/**
 * EDIT MODE ENABLING ALL INPUTS
 * SHOWING SAVE BUTTON HIDING EDIT BUTTON
 */
function editModeSpotDetails() {
    $('#details-general-form :input:not(#town-details-disable):not(#address-details-disable):not(#longitude-details-disable):not(#latitude-details-disable):not(button)').prop("disabled", false);
    $("#edit-details-info").hide();
    $("#save-details-info").removeClass('hide');
    $("#save-details-info").show();
}

/**
 * UPDATE SPOT EMIT MODIFICATIONS TO SOCKET.IO
 * @param {string} idSpot
 */
function saveModeSpotDetails(idSpot) {
    if ($("#details-general-form").valid()) {
        var address = document.getElementById('address-details-disable').value;
        var town = document.getElementById('town-details-disable').value;
        var longitude = document.getElementById('longitude-details-disable').value;
        var latitude = document.getElementById('latitude-details-disable').value;
        var number = document.getElementById('number-details-disable').value;
        var price = document.getElementById('price-details-disable').value;
        var description = document.getElementById('description-details-disable').value;

        socket.emit('update_spot', idSpot, address, town, longitude, latitude, number, price, description);

        $('#details-general-form :input:not(button)').prop("disabled", true);
        $('#details-general-form :input:not(button)').removeClass("valid");
        $("#edit-details-info").show();
        $("#save-details-info").addClass('hide');
        $("#save-details-info").hide();
    }
}

//-----------------------------SECOND SECTION DISPONIBILITY -----------------------------------------------------------------------
var $spot;
var $idSpot;

/**
 * add disponibility with values from form
 * @param {string} idSpot
 */
function addDisponibility(idSpot) {
    if ($("#details-dispo-form").valid()) {
        var date = document.getElementById('details-spot-date').value;
        var startHour = document.getElementById('details-spot-starthour').value + ":00";
        var endHour = document.getElementById('details-spot-endhour').value + ":00";
        var frequency = document.getElementById('details-frequency').value;

        socket.emit('add_disponibility_spot', idSpot, date, startHour, endHour, frequency);
        $("#details-dispo-form").trigger('reset');
    }
}

function getCurrentSpot() {
    socket.emit('get_spot_details', $idSpot);
}

function renderCalendarEvents() {
    $spot.Disponibilities.forEach((dispo) => {
        $('#calendar.disponibility-list-calendar').fullCalendar('renderEvent',
            {
                title: "Disponible",
                start: dispo.date + " " + dispo.startHour,
                end: dispo.date + " " + dispo.endHour,
                id: dispo.id,
                startHour: dispo.startHour,
                endHour: dispo.endHour,
                date: dispo.date
            },
            true // make the event "stick"
        );
    })
}

function renderCalendar() {

    $('#calendar.disponibility-list-calendar').fullCalendar('destroy');

    $('#calendar.disponibility-list-calendar').fullCalendar({
        defaultView: 'listWeek',
        timeFormat: 'HH:mm',
        lang: 'fr-ch',
        noEventsMessage: 'Aucune disponibilité aujourd\'hui',
        eventClick: function (calEvent, jsEvent, view) {
            var result = confirm('Voulez-vous supprimer cette disponibilité ? ' + calEvent.date + ' de ' + calEvent.startHour + " à " + calEvent.endHour);
            if (result) {
                //delete the current disponibility
                socket.emit('delete_disponibility', calEvent.id);
            }
        }
    })

    $('.fc button').removeClass('fc-button fc-state-default');
    $('.fc button').addClass('btn waves-effect waves-light');

    renderCalendarEvents();
}

function displayDisponibilities() {
    getCurrentSpot();
    renderCalendar();
}

//-----------------------------THIRD SECTION BOOKINGS -----------------------------------------------------------------------

function deleteBooking(idBooking) {
    socket.emit('delete_booking', idBooking);
}






