window['moment-range'].extendMoment(moment);

var socket = io();
var $map;
var $defaultOptions = {center: {lat: 46.292781, lng: 7.536189}, zoom: 13, mapTypeId: 'roadmap'}

var $autocomplete;
var $input;
var $searchButton;
var $searchBox;
var $searchForm;
var $spot;
/*INPUT VALUES */
var $date;
var $startHour;
var $endHour;
var $address;
/*MARKERS*/
var $markers = [];

$(document).ready(function () {
    initMap();

    $searchButton.onclick = function () {
        if ($("#spots-form-search").valid()) {
            clearAllMarkers();
            requestAvailableSpots();
        }
    }
})

socket.on('available_spots_research_result', function (spotsList) {
    searchAvailableSpots(spotsList);
})

socket.on('available_spots_research_result_fail', function (error) {
    var toastHTML = '<span>Aucune place ne parc disponible à cette date</span><button class="btn-flat toast-action">' + error + '</button>';
    M.toast({html: toastHTML});
});

socket.on('book_spot_result', function () {
    var toastHTML = '<span>Votre réservation a bien été effectuée</span><button class="btn-flat toast-action"><i class="material-icons">check</i></button>';
    M.toast({ html: toastHTML });
    window.location.href = '/admin/profile#profile-books';

})


function initMap() {
    //LOADING UI ELEMENTS
    $input = document.getElementById('spots-address-search');
    $searchButton = document.getElementById('spots-submit-search-button');
    $searchForm = document.getElementById('spots-form-search');
    //LOADING GOOGLE MAP FUNCTIONS
    $autocomplete = new google.maps.places.Autocomplete($input);
    $searchBox = new google.maps.places.SearchBox($input);
    //LOADING GOOGLE MAP
    $map = new google.maps.Map(document.getElementById('map'), $defaultOptions);
    //TESTING IF GOOGLE MAP IS AVAILABLE IN THE BROWSER
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var $position = {lat: position.coords.latitude, lng: position.coords.longitude};
            $map.panTo($position);

            var marker = new google.maps.Marker(
                {
                    position: $position,
                    map: $map,
                    icon:
                        {
                            url: "/images/icons/location.svg",
                            scaledSize: new google.maps.Size(43, 43)
                        }

                });
            marker.setAnimation(google.maps.Animation.DROP);

            closeLoading();


        }, function () {
            //GEOLOCALISATION ERROR USE DEFAULT LOCALISATION
            $map.panTo($defaultOptions.center);

            var marker = new google.maps.Marker(
                {
                    position: $defaultOptions.center,
                    map: $map,
                    icon:
                        {
                            url: "/images/icons/location.svg",
                            scaledSize: new google.maps.Size(43, 43)
                        },
                    animation: google.maps.Animation.DROP
                });

            closeLoading();

        }, {timeout: 10000});
    } else {
        //BROWSER DON'T SUPPORT GEOLOCALISATION
        handleLocationError(false, infoWindow, $map.getCenter());

    }
}

/*
call the method to search available spots with the inputs
 */
function requestAvailableSpots() {
    $date = $('#spots-date-search').val();
    $startHour = $('#spots-start-hour-search').val() + ":00";
    $endHour = $('#spots-end-hour-search').val() + ":00";
    $address = $('#spots-address-search').val();
    $idUser = $('#search-user-id').val();
    socket.emit('available_spots_research', $date, $startHour, $endHour, $address, $idUser);
}

/*
insert markers on the available spots
 */
function searchAvailableSpots(spotsList) {

    var address = $input.value;

    new google.maps.Geocoder().geocode({'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            $map.panTo(results[0].geometry.location);
            smoothZoom(15, $map.getZoom());

            var marker = new google.maps.Marker({
                map: $map,
                position: results[0].geometry.location,
                icon:
                    {
                        url: "/images/icons/destination.svg",
                        scaledSize: new google.maps.Size(43, 43)
                    }
            });

            $markers.push(marker);

            spotsList.forEach((spot) => {
                var $resultSpotGeolocalisation = {lat: spot.latitude, lng: spot.longitude};
                //CREATE A MARKER FOR THE AVAILABLE SPOT
                if (!spot.available)
                    var marker = new google.maps.Marker({
                        position: $resultSpotGeolocalisation,
                        map: $map,
                        icon:
                            {
                                url: "/images/icons/occupied.svg",
                                scaledSize: new google.maps.Size(43, 43)
                            }
                    });
                else
                    var marker = new google.maps.Marker({
                        position: $resultSpotGeolocalisation,
                        map: $map,
                        icon:
                            {
                                url: "/images/icons/available.svg",
                                scaledSize: new google.maps.Size(43, 43)
                            },
                        animation: google.maps.Animation.BOUNCE
                    });


                //ADD A CLICK LISTNER FOR EVERY MARKER
                marker.addListener('click', function () {
                    var $modalResultSpot = document.querySelector('#modal-result-spot');
                    var $modalResultSpotInstance = M.Modal.init($modalResultSpot);
                    renderSpotCardView(spot);
                    renderSpotCalendarEvents(spot);
                    $modalResultSpotInstance.open();
                });

                $markers.push(marker);
            });


        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

/*
adjust the zoom on the map
 */
function smoothZoom(targetZoom, currentZoom) {

    if (currentZoom >= targetZoom)
        return;
    else {
        zoom = google.maps.event.addListener($map, 'zoom_changed', function (event) {
            google.maps.event.removeListener(zoom);
            smoothZoom(targetZoom, currentZoom + 1);
        });

        setTimeout(function () {
            $map.setZoom(currentZoom)
        }, 80); // 80ms
    }
}

/*
clear markers on the map
 */
function clearAllMarkers() {
    $markers.forEach(function (marker) {
        marker.setMap(null);
    });
}
function renderSpotCardView(spot) {

    //SET SPOT MODAL VALUES
    $locationIcon = "<i class='material-icons material-icon-vertical-align'>location_on</i>";
    $priceIcon = "<i class='material-icons material-icon-vertical-align'>payment</i>";
    $gpsIcon = "<i class='material-icons material-icon-vertical-align'>map</i>";
    $('.card-image img').attr("src", `/spots/${spot.picture}`)
    $('#modal-result-spot-title > p').text(spot.address + " n° " + spot.number);
    $('#modal-result-spot-description').text(spot.description);
    $('#modal-result-spot-town').html($locationIcon + "<span class='tabulation1'>localité</span><span class='tabulation2'>:</span><span class='tabulation3'>" + spot.Town.name + "</span>");
    $('#modal-result-spot-price').html($priceIcon + "<span class='tabulation1' >taux horaire</span><span class='tabulation2'>:</span><span class='tabulation3'>" + spot.price + " CHF</span>")
    $('#modal-result-spot-gps').html($gpsIcon + "<span class='tabulation1' >gps</span><span class='tabulation2'>:</span><span class='tabulation3'>" + spot.latitude+ " "+ spot.longitude+" </span>")
    //SET BOOKING MODAL VALUES
    if (spot.available) {
        $('#spots-date-booking').val($date);
        $('#spots-start-hour-booking').val($startHour);
        $('#spots-end-hour-booking').val($endHour);
        $('#spots-address-booking').val(spot.address);
        var start = moment($date + " " + $startHour);
        var end = moment($date + " " + $endHour);
        var duration = end.diff(start, 'minutes');
        //calculate the price and round with two digits
        var price = Math.round((duration * spot.price / 60) * 100) / 100;
        //get only digits for the price if needed
        if (price % 1 != 0) {
            var priceArray = price.toString().split('.');
            var digits = Number(priceArray[1]);
            if (digits % 5 != 0) {
                priceArray[1] = (digits + (5 - (digits % 5))).toString();
                price = (priceArray[0] + '.' + priceArray[1]);
            }
        }
        $('#spots-total-booking').val(Number(price));
        $('#horizontal-activator-booking').show();
    }
    else
        $('#horizontal-activator-booking').hide();
}

function renderSpotCalendarEvents(spot) {
    $spot = spot;

    $('#calendar.modal-result-spot-disponibilities').fullCalendar('destroy');

    spot.Disponibilities.forEach((disponibility) => {
        var $remainingDisponibilities;
        var $bookings = [];
        var $occupiedRanges = [];

        var $startDisponibility = moment(disponibility.date + " " + disponibility.startHour);
        var $endDisponibility = moment(disponibility.date + " " + disponibility.endHour);
        var $rangeDisponibility = moment.range($startDisponibility, $endDisponibility);


        disponibility.Bookings.forEach((booking) => {

            var $startBooking = moment(disponibility.date + " " + booking.startHour);
            var $endBooking = moment(disponibility.date + " " + booking.endHour);
            var $rangeBooking = moment.range($startBooking, $endBooking);
            $occupiedRanges.push($rangeBooking);
            $bookings.push($rangeBooking);


        })

        $remainingDisponibilities = subtractRanges($rangeDisponibility, $occupiedRanges);
        renderCalendar($remainingDisponibilities, $bookings);


    })

}

function renderCalendar($remainingDisponibilities, $bookings) {

    $('#calendar.modal-result-spot-disponibilities').fullCalendar({
        defaultView: 'listWeek',
        timeFormat: 'HH:mm',
        lang: 'fr-ch',
        noEventsMessage: 'Aucune disponibilité aujourd\'hui'
    })
    $('.fc button').removeClass('fc-button fc-state-default');
    $('.fc button').addClass('btn waves-effect waves-light');

    $remainingDisponibilities.forEach((disponibility) => {
        $('#calendar.modal-result-spot-disponibilities').fullCalendar('renderEvent',
            {
                title: "Libre",
                start: disponibility.start.toDate(),
                end: disponibility.end.toDate()
            },
            true // make the event "stick"
        );
    })

    $bookings.forEach((booking) => {
        $('#calendar.modal-result-spot-disponibilities').fullCalendar('renderEvent',
            {
                title: "Occupé",
                start: booking.start.toDate(),
                end: booking.end.toDate()
            },
            true // make the event "stick"
        );
    })

}

/*
get the informations of the customer who wants to book and send it
 */
function bookSpot() {
    let $bookObject = {};
    let $sessionUser = JSON.parse(document.getElementById('sessionUser').value);
    let $finalPrice = document.getElementById('spots-total-booking').value;
    $bookObject.idUser = $sessionUser.id;
    $bookObject.firstname = $sessionUser.firstname;
    $bookObject.email = $sessionUser.email;
    $bookObject.startHour = $startHour;
    $bookObject.endHour = $endHour;
    $bookObject.date = $date;
    $bookObject.spot = $spot;
    $bookObject.finalPrice = $finalPrice;

    socket.emit('book_spot', $bookObject);
    $('#modal-result-spot').modal('close');
}

/*
close loading page
 */
function closeLoading() {
    $('#search-loading-overlay').hide();
}

function subtractRanges(longRanges, shortRanges) {
    // Always return an array
    if (shortRanges.length === 0)
        return longRanges.hasOwnProperty("length")
            ? longRanges
            : [longRanges];

    // Result is empty range
    if (longRanges.length === 0)
        return [];

    if (!longRanges.hasOwnProperty("length"))
        longRanges = [longRanges];

    for (let long in longRanges) {
        for (let short in shortRanges) {
            longRanges[long] = longRanges[long].subtract(shortRanges[short]);
            if (longRanges[long].length === 0) {
                // Subtracted an entire range, remove it from list
                longRanges.splice(long, 1);
                shortRanges.splice(0, short);
                return this.subtractRanges(longRanges, shortRanges);
            }
            else if (longRanges[long].length === 1) {
                // No subtraction made, but .subtract always returns arrays
                longRanges[long] = longRanges[long][0];
            }
            else {
                // Successfully subtracted a subrange, flatten and recurse again
                const flat = [].concat(...longRanges);
                shortRanges.splice(0, short);
                return this.subtractRanges(flat, shortRanges);
            }
        }
    }
    return longRanges;
}

