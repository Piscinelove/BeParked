var socket = io();
socket.on("update_user_result", function (user) {
    var toastHTML = '<span>Profil modifié avec succès</span>';
    M.toast({html: toastHTML});
});

socket.on("delete_spot_result", function (result) {
    var toastHTML = '<span>' + result + '</span>';
    M.toast({html: toastHTML});
    $('#profile-spots').load(' #profile-spots');
});

$(document).ready(function () {

    defaultModeProfile();

});

/*
update a profile with an id user
 */
function updateProfile(idUser) {

    if ($("#form-profile-personaldata").valid()) {
        let $firstname = document.getElementById('firstname-profile-disable').value;
        let $lastname = document.getElementById('lastname-profile-disable').value;
        let $address = document.getElementById('address-profile-disable').value;
        let $town = document.getElementById('town-profile-disable').value;
        let $phone = document.getElementById('phone-profile-disable').value;
        let $password = document.getElementById('psw2-profile').value;
        let $licensePlate = document.getElementById('car-profile-disable').value;
        let $brand = document.getElementById('model-profile-disable').value;
        socket.emit("update_user", idUser, $firstname, $lastname, $address, $town, $phone, $password, $licensePlate, $brand);

        $('#form-profile-personaldata :input:not(#email-profile-disable):not(button)').prop("disabled", true);
        $('#form-profile-personaldata :input:not(#email-profile-disable):not(button)').removeClass("valid");
        $("#edit-profile-info").show();
        $("#save-profile-info").addClass('hide');
        $("#save-profile-info").hide();
        $("#password-row").addClass('hide');
        $("#password-row").hide();
    }
}

/*
Delete Spot by an id
 */
function deleteSpot(idSpot) {
    socket.emit("delete_spot", idSpot);
}

/*
enable edit mode on profil informations
 */
function editModeProfile() {
    $('#form-profile-personaldata :input:not(#email-profile-disable):not(button)').prop("disabled", false);
    $("#edit-profile-info").hide();
    $("#save-profile-info").removeClass('hide');
    $("#save-profile-info").show();
    $("#password-row").removeClass('hide');
    $("#password-row").show();


}

/*
disable edit mode on profil information
 */
function defaultModeProfile() {
    $('#form-profile-personaldata :input:not(#email-profile-disable):not(button)').prop("disabled", true);
    $("#edit-profile-info").show();
    $("#password-row").hide();
    $("#save-profile-info").hide();

}
