$(document).ready(function () {
    $('.sidenav#admin-nav').sidenav();
    $(".dropdown-trigger").dropdown();
    $('.modal').modal();
    $('.collapsible').collapsible();

    $('#spots-link-tab').on("click", function () {
        var instance = M.Tabs.getInstance($('#profile-tabs'));
        instance.select('profile-spots');
    })

    $('#books-link-tab').on("click", function () {
        var instance = M.Tabs.getInstance($('#profile-tabs'));
        instance.select('profile-books');
    })

    $('#profile-link-tab').on("click", function () {
        var instance = M.Tabs.getInstance($('#profile-tabs'));
        instance.select('profile-personaldata');
    })


});

