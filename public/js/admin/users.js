var socket = io();

/**
 * wait on server response for spot update result
 */
socket.on('create_admin_result', (result) => {
    console.log("result");
    var toastHTML = '<span>' + result + '</span>';
    M.toast({html: toastHTML});
    $('#users-table').load(' #users-table');
})

/**
 * wait on server response for delete user
 */
socket.on('delete_user_result', function (result) {
    var toastHTML = '<span>' + result + '</span>';
    M.toast({html: toastHTML});
    $('#users-table').load(' #users-table');
})


/**
 * Admin creation
 */
function createAdmin() {
    var $firstname = document.getElementById('firstname-add').value;
    var $lastname = document.getElementById('lastname-add').value;
    var $address = document.getElementById('address-add').value;
    var $town = document.getElementById('town-add').value;
    var $email = document.getElementById('email-add').value;
    var $phone = document.getElementById('phone-add').value;
    var $password = document.getElementById('psw1-add').value;

    if ($("#form-add-user").valid()) {
        //emit the socket
        socket.emit('create_admin', $firstname, $lastname, $address, $town, $email, $phone, $password);

        //close the modal
        $('#add_user').modal('close');
    }
};

function deleteUser(idUser) {
    socket.emit('delete_user', idUser);
};