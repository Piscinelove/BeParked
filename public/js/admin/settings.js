var socket = io();
var $about;
var $conditions;

socket.on('save_about_text_result', function (text) {
    var toastHTML = '<span>Votre modification a bien été sauvegardée</span><button class="btn-flat toast-action"><i class="material-icons">check</i></button>';
    M.toast({ html: toastHTML });
})

socket.on('save_conditions_text_result', function (text) {
    var toastHTML = '<span>Votre modification a bien été sauvegardée</span><button class="btn-flat toast-action"><i class="material-icons">check</i></button>';
    M.toast({ html: toastHTML });
})

socket.on('get_about_text_result', async (text) => {
    $about.container.firstChild.innerHTML = text;
})

socket.on('get_conditions_text_result', async (text) => {
    $conditions.container.firstChild.innerHTML = text;
})

$(document).ready(function () {
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],

        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction

        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'align': [] }],

        ['clean']                                         // remove formatting button
    ];
    var options = {
        modules: {
            toolbar: toolbarOptions
        },
        readOnly: false,
        theme: 'snow'
    };

    var containerAbout = document.getElementById('settings-about-editor');
    $about = new Quill(containerAbout, options);
    socket.emit('get_about_text');

    var containerConditions = document.getElementById('settings-conditions-editor');
    $conditions = new Quill(containerConditions, options);
    socket.emit('get_conditions_text');
})

function saveAbout() {
    socket.emit('save_about_text', $about.container.firstChild.innerHTML);
}

function saveConditions() {
    socket.emit('save_conditions_text', $conditions.container.firstChild.innerHTML);
}
