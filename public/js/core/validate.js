$(document).ready(function(){
    validateForms();
})


/**
 * Validation of all the forms
 */
function validateForms() {

    /*********** METHOD **************/

    //validation swiss licence plate
    $.validator.addMethod("isSwissLicencePlate", function (value, element) {

        //array of the swiss "canton"
        var plate = $('.swiss-plate').val();

        if(plate == '')
            return true
        else
            return /(AG)([0-9]{1,6})$|(AI)([0-9]{1,5})$|(AR)([0-9]{1,5})$|(BE)([0-9]{1,6})$|(BL)([0-9]{1,6})$|(BS)([0-9]{1,6})$|(FR)([0-9]{1,6})$|(GE)([0-9]{1,6})$|(GL)([0-9]{1,5})$|(GR)([0-9]{1,6})$|(JU)([0-9]{1,5})$|(LU)([0-9]{1,6})$|(NE)([0-9]{1,6})$|(NW)([0-9]{1,5})$|(OW)([0-9]{1,6})$|(SG)([0-9]{1,6})$|(SH)([0-9]{1,5})$|(SO)([0-9]{1,6})$|(SZ)([0-9]{1,6})$|(TG)([0-9]{1,6})$|(TI)([0-9]{1,6})$|(UR)([0-9]{1,6})$|(VD)([0-9]{1,6})$|(VS)([0-9]{1,6})$|(ZG)([0-9]{1,6})$|(ZH)([0-9]{1,6})$/
                .test(plate.replace(/\s+/g, '').toUpperCase());
    });


    //validation swiss phone
    $.validator.addMethod("isSwissPhoneNumber", function(value, element) {
        return /^(0041|041|\+41|\+\+41|41)?(0|\(0\))?([1-9]\d{1})(\d{3})(\d{2})(\d{2})$/
            .test($(element).val().replace(/\s+/g, '')); // return true if field is ok or should be ignored
    });



    //validation of the town
    $.validator.addMethod("isValidTown", function(value, element) {
        var townsList = [];

        //Remote refactoring
        $.ajax({
            type: "GET",
            url: "/autocomplete/name="+$(element).val(),
            async:false,
            success:function (data) {
                $.each(data, function( id, val ) {
                    townsList.push(val.name);
                });
            }
        })
        return townsList.indexOf($(element).val()) !== -1;
    });


    //validation of the start time before end time
    $.validator.addMethod("isValidTime", function(value, element) {
        var start = $('.time-start').val();
        var end = $('.time-end').val();

        if(end <= start)
            return false;
        else
            return true;
    });

    //validation charactere phone
    $.validator.addMethod("isValidChar", function(value, element) {

        var price = $('.price').val();


        price = price.replace(',','.');
        return /^(\d{1,})(.\d[05]{1,2})?$/g
            .test(price);
    });

    //validation frequency not negative
    $.validator.addMethod("isValidNotNegative", function(value, element) {

        var num = $('.positive').val();

        if(num == '')
            return true
        else if(num <1)
            return false;
        else
            return true;

    });




    /******** VALIDATION FORM ************/

    //add user
    $("#form-add-user").validate({
        rules:{
            firstname:{
                required:true,
                minlength:2
            },
            lastname:{
                required:true,
                minlength:2
            },
            address: {
                required:true,
                minlength:3
            },
            town: {
                required:true,
                isValidTown:true,
                minlength:2
            },
            email:{
                required:true,
                minlength:4
            },
            phone:{
                required:true,
                isSwissPhoneNumber:true,
                minlength:3
            },
            password:{
                required:true,
                minlength:2
            },
            password2:{
                required:"psw1-add:filled",
                minlength:2,
                equalTo:"#psw1-add"
            }
        },
        messages:{
            firstname:{
                required: "Veuillez saisir votre prénom",
                minlength:"Minimum 2 caractères"
            },
            lastname:{
                required:"Veuillez saisir votre nom",
                minlength:"Minimum 2 caractères"
            },
            address: {
                required:"Veuillez saisir votre adresse",
                minlength:"Minimum 3 caractères"
            },
            town: {
                required:"Veuillez saisir votre ville",
                isValidTown:"Veuillez saisir une ville valide",
                minlength:"Minimum 2 caractères"
            },
            email:{
                required:"Veuillez saisir votre email",
                minlength:"Minimum 4 caractères",
                email : "Veuillez saisir un email correct."
            },
            phone:{
                required:"Veuillez saisir votre n° de téléphone",
                isSwissPhoneNumber:"Veuillez saisir un n° de téléphone valide",
                minlength:"Minimum 3 caractères"
            },
            password:{
                required:"Veuillez saisir un mot de passe",
                minlength: "Minimum 2 caractères"
            },
            password2:{
                required:"Veuillez confirmer le mot de passe",
                minlength:"Minimum 2 caractères",
                equalTo:"Veuillez saisir un mot de passe identique"
            }
        },
        errorElement : 'span'
    });

    //spot details
    $("#details-general-form").validate({
        rules:{
            address: {
                required:true,
                minlength:2
            },
            town: {
                required:true,
                isValidTown:true,
                minlength:2
            },
            number:{
                isValidNotNegative:true,
            },
            price:{
                required: true,
                isValidChar: true,
                minlength:1
            }
        },
        messages:{
            address:{
                required:"Veuillez saisir une adresse",
                minlength:"Minimum 2 caractères"
            },
            town:{
                required:"Veuillez saisir un ville",
                isValidTown:"Veuillez saisir une ville valide",
                minlength:"Minimum 2 caractères"
            },
            number:{
                isValidNotNegative:"Veuillez saisir un n° de place positif",
            },
            price: {
                required: "Veuillez saisir un prix par heure",
                isValidChar: "Veuillez insérer un prix avec deux chiffres après la virgule (ex. 1.20)",
                minlength: "Minimum 1 caractère"
            }
        },
        errorElement : 'span'
    });

    //details dispo
    $('#details-dispo-form').validate({
        rules: {

            date:{
                required:true
            },
            timestart:{
                required:true,
            },
            timeend:{
                required:true,
                isValidTime:true
            },
            frequency:{
                required:true,
                isValidNotNegative:true
            }
        },
        messages: {
            date:{
                required: "Veuillez saisir une date",
            },
            timestart:{
                required:"Veuillez saisir une heure de début",
            },
            timeend:{
                required:"Veuillez saisir une heure de fin",
                isValidTime:"L'heure de fin doit être après l'heure de début"
            },
            frequency:{
                required:"Veuillez saisir une fréquence (min. 1)",
                isValidNotNegative:"Veuillez saisir une fréquence positive (min. 1) ",
                min:"Veuillez saisir une fréquence positive (min. 1)"
            }
        },

        errorElement : 'span'
    });




    //profil view
    $("#form-profile-personaldata").validate({
        rules:{
            firstname:{
                required:true,
                minlength:2
            },
            lastname:{
                required:true,
                minlength:2
            },
            address: {
                required:true,
                minlength:3
            },
            town: {
                required:true,
                isValidTown:true,
                minlength:2
            },
            email:{
                required:true,
                minlength:4
            },
            phone:{
                required:true,
                isSwissPhoneNumber:true,
                minlength:3
            },
            plate:{
                isSwissLicencePlate:true,
                minlength:3
            },
            model:{
                minlength:2

            },
            password:{
                minlength:2
            },
            password2:{
                required:"psw1-profile:filled",
                minlength:2,
                equalTo:"#psw1-profile"
            }
        },
        messages:{
            firstname:{
                required: "Veuillez saisir votre prénom",
                minlength:"Minimum 2 caractères"
            },
            lastname:{
                required:"Veuillez saisir votre nom",
                minlength:"Minimum 2 caractères"
            },
            address: {
                required:"Veuillez saisir votre adresse",
                minlength:"Minimum 3 caractères"
            },
            town: {
                required:"Veuillez saisir votre ville",
                isValidTown:"Veuillez saisir une ville valide",
                minlength:"Minimum 2 caractères"
            },
            email:{
                required:"Veuillez saisir votre email",
                minlength:"Minimum 4 caractères",
                email : "Veuillez saisir un email correct."
            },
            phone:{
                required:"Veuillez saisir votre n° de téléphone",
                isSwissPhoneNumber:"Veuillez saisir un n° de téléphone valide",
                minlength:"Minimum 3 caractères"
            },
            plate:{
                isSwissLicencePlate:"Veuillez saisir un n° d'immatriculation valide",
                minlength:"Minimum 3 caractères"
            },
            model: {
                minlength: "Minimum 2 caractères"
            },
            password:{
                minlength: "Minimum 2 caractères"
            },
            password2:{
                required:"Veuillez confirmer le mot de passe",
                minlength:"Minimum 2 caractères",
                equalTo:"Veuillez saisir un mot de passe identique"
            }
        },
        errorElement : 'span'
    });

    //spot view
    $("#form-spot-add").validate({
        rules:{
            address:{
                required:true,
                minlength:2
            },
            town:{
                required:true,
                isValidTown:true,
                minlength:2
            },
            number:{
                isValidNotNegative:true,
            },
            price:{
                required: true,
                isValidChar:true,
                minlength:1
            },
            image: {
                required: true
            }
        },
        messages:{
            address:{
                required:"Veuillez saisir une adresse",
                minlength:"Minimum 2 caractères"
            },
            town:{
                required:"Veuillez saisir un ville",
                isValidTown:"Veuillez saisir une ville valide",
                minlength:"Minimum 2 caractères"
            },
            number:{
                isValidNotNegative:"Veuillez saisir un n° de place positif"
            },
            price:{
                required: "Veuillez saisir un prix par heure",
                isValidChar:"Veuillez insérer un prix avec deux chiffres après la virgule (ex. 1.20)",
                minlength:"Minimum 1 caractère"
            },
            image: {
                required:"Veuillez charger une image"
            }
        },
        errorElement : 'span'
    });

    //search view
    $("#spots-form-search").validate({
        rules:{
            date:{
                required:true
            },
            timestart:{
                required:true,
            },
            timeend:{
                required:true,
                isValidTime:true
            },
            city:{
                required:true,
                minlength:2
            },
        },
        messages:{
            date:{
                required: "Veuillez saisir une date",
            },
            timestart:{
                required:"Veuillez saisir une heure de début",
            },
            timeend:{
                required:"Veuillez saisir une heure de fin",
                isValidTime:"L'heure de fin doit être après l'heure de début"
            },
            city:{
                required:"Veuillez saisir un ville",
                minlength:"Minimum 2 caractères"
            },
        },
        errorElement : 'span'

    });

    //login view login
    $("#form-login").validate({
        rules: {
            email: {
                required: true,
                minlength: 4
            },
            password: {
                required: true,
                minlength: 2
            },
        },
        //For custom messages
        messages: {
            email:{
                required: "Veuillez saisir un email valide",
                minlength:"Minimum 4 caractères",
                email : "Veuillez saisir un email correct."
            },
            password:{
                required:"Veuillez saisir un mot de passe",
                minlength: "Minimum 2 caractères"
            }
        },
        errorElement : 'span',
        errorContainer : '.error-message',
        errorLabelContainer : '.error-message ul',
        wrapper: 'li'
    });


    //login view register
    $("#form-register").validate({
        rules:{
            firstname:{
                required:true,
                minlength:2
            },
            lastname:{
                required:true,
                minlength:2
            },
            address: {
                required:true,
                minlength:3
            },
            town: {
                required:true,
                isValidTown:true,
                minlength:2
            },
            email:{
                required:true,
                minlength:4
            },
            phone:{
                required:true,
                isSwissPhoneNumber:true,
                minlength:3
            },
            plate:{
                isSwissLicencePlate:true,
                minlength:3
            },
            model:{
                minlength:2
            },
            password:{
                required:true,
                minlength:2
            },
            password2:{
                required:"psw1-register:filled",
                minlength:2,
                equalTo:"#psw1-register"
            },
            conditions:{
                required:true
            }
        },
        messages:{
            firstname:{
                required: "Veuillez saisir votre prénom",
                minlength:"Minimum 2 caractères"
            },
            lastname:{
                required:"Veuillez saisir votre nom",
                minlength:"Minimum 2 caractères"
            },
            address: {
                required:"Veuillez saisir votre adresse",
                minlength:"Minimum 3 caractères"
            },
            town: {
                required:"Veuillez saisir votre ville",
                isValidTown:"Veuillez saisir une ville valide",
                minlength:"Minimum 2 caractères"
            },
            email:{
                required:"Veuillez saisir votre email",
                minlength:"Minimum 4 caractères",
                email : "Veuillez saisir un email correct."
            },
            phone:{
                required:"Veuillez saisir votre n° de téléphone",
                isSwissPhoneNumber:"Veuillez saisir un n° de téléphone valide",
                minlength:"Minimum 3 caractères"
            },
            plate:{
                isSwissLicencePlate:"Veuillez saisir un n° d'immatriculation valide",
                minlength:"Minimum 3 caractères"
            },
            model:{
                minlength:"Minimum 2 caractères"
            },
            password:{
                required:"Veuillez saisir un mot de passe",
                minlength: "Minimum 2 caractères"
            },
            password2:{
                required:"Veuillez confirmer le mot de passe",
                minlength:"Minimum 2 caractères",
                equalTo:"Veuillez saisir un mot de passe identique"
            },
            conditions:{
                required:"Veuillez accepter les conditions générales"
            }
        },
        errorElement : 'span',
        errorContainer : '.error-message',
        errorLabelContainer : '.error-message ul',
        wrapper: 'li'
    });



    //contact form - index
    $("#form-contact").validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                minlength: 2
            },
            object: {
                required: true,
                minlength: 2
            },
            description: {
                required: true,
                minlength: 4
            },
        },
        //For custom messages
        messages: {
            name:{
                required: "Veuillez saisir votre nom et prénom",
                minlength:"Minimum 2 caractères"
            },
            email:{
                required:"Veuillez saisir e-mail valide",
                minlength: "Minimum 2 caractères",
                email : "Veuillez saisir un email correct."
            },
            object:{
                required:"Veuillez saisir un objet",
                minlength: "Minimum 2 caractères"
            },
            description:{
                required:"Veuillez saisir votre message",
                minlength: "Minimum 4 caractères"
            }
        },
        errorElement : 'span'
    });

}
