$(document).ready(function(){

    $('.datepicker').datepicker(
        {

            format:'yyyy-mm-dd',
            minDate:new Date(),
            defaultDate:new Date(),
            setDefaultDate:new Date(),
            i18n:
            {
                cancel: 'Fermer',
                clear: 'Annuler',
                done: 'Ok',
                previousMonth : '‹',
                nextMonth     : '›',
                months: [ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
                    "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" ],
                monthsShort: [ "Janv.", "Févr.", "Mars", "Avril", "Mai", "Juin",
                    "Juil.", "Août", "Sept.", "Oct.", "Nov.", "Déc." ],
                weekdays: [ "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" ],
                weekdaysShort: [ "Dim.", "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam." ],
                weekdaysAbbrev: [ "D", "L", "M", "M", "J", "V", "S" ]
            },
            onClose:function (arg) {
                $('.datepicker-valid-error').valid();
            }
        }
    );


    $('.timepicker').timepicker(
        {
            twelveHour:false,
            defaultTime:'now',
            i18n:
                {
                    cancel: 'Fermer',
                    clear: 'Annuler',
                    done: 'Ok'
                },
            onClose:function (arg) {
                $('.timepicker-valid-error').valid();
            }
        });
    $('#horizontal-back-activator-result').click(function () {
        $('#modal-result-spot').modal('close');
    })
    $('#horizontal-activator-disponibilities').click(function () {
        $('.card-reveal-disponibilities').css('transform','translateY(-100%)');
        $('.card-reveal-disponibilities').css('-moz-transform','translateY(-100%)');
        $('.card-reveal-disponibilities').css('-o-transform','translateY(-100%)');
        $('.card-reveal-disponibilities').css('-webkit-transform','translateY(-100%)');
    })

    $('#horizontal-back-activator-disponibilities').click(function () {
        $('.card-reveal-disponibilities').css('transform','translateY(0%)');
        $('.card-reveal-disponibilities').css('-moz-transform','translateY(0%)');
        $('.card-reveal-disponibilities').css('-o-transform','translateY(0%)');
        $('.card-reveal-disponibilities').css('-webkit-transform','translateY(0%)');
    })

    $('#horizontal-activator-booking').click(function () {
        $('.card-reveal-booking').css('transform','translateY(-100%)');
        $('.card-reveal-booking').css('-moz-transform','translateY(-100%)');
        $('.card-reveal-booking').css('-o-transform','translateY(-100%)');
        $('.card-reveal-booking').css('-webkit-transform','translateY(-100%)');
    })

    $('#horizontal-back-activator-booking').click(function () {
        $('.card-reveal-booking').css('transform','translateY(0%)');
        $('.card-reveal-booking').css('-moz-transform','translateY(0%)');
        $('.card-reveal-booking').css('-o-transform','translateY(0%)');
        $('.card-reveal-booking').css('-webkit-transform','translateY(0%)');
    })

    $('.tabs').tabs();
    $('.modal').modal();

    $('.parallax').parallax();

    $('textarea#contact-description').characterCounter();


});
