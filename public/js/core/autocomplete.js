$(document).ready(function () {
    $('input.autocomplete').on("input", function (event) {
        var input = event.target.value;
        $.ajax({
            type: 'GET',
            url: "/autocomplete/name=" + input,
            success: function (towns) {
                var data = {};
                $.each(towns, function (id, val) {
                    data[val.name] = null;
                })

                $(event.target).autocomplete({
                    data: data,
                    limit: 5, // The max amount of results that can be shown at once. Default: Infinity.
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
            }
        });
    })
});