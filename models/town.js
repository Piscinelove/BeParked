'use strict';
module.exports = (sequelize, DataTypes) => {
  var Town = sequelize.define('Town', {
    name: DataTypes.STRING
  }, {});
  Town.associate = function(models) {
      Town.hasMany(models.User, {foreignKey: 'idTown'});
      Town.belongsTo(models.Canton, {foreignKey: 'idCanton'});
      Town.hasMany(models.Spot, {foreignKey: 'idTown'});
  };
  return Town;
};