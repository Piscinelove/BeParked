'use strict';
module.exports = (sequelize, DataTypes) => {
  var Disponibility = sequelize.define('Disponibility', {
    date: DataTypes.DATEONLY,
    startHour: DataTypes.TIME,
    endHour: DataTypes.TIME
  }, {});
  Disponibility.associate = function(models) {
      Disponibility.belongsTo(models.Spot, {onDelete:'CASCADE',hooks: true, foreignKey: 'idSpot'});
      Disponibility.hasMany(models.Booking, {onDelete:'CASCADE',hooks: true,foreignKey: 'idDisponibility'});

  };
  return Disponibility;
};