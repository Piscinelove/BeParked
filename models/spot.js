'use strict';
module.exports = (sequelize, DataTypes) => {
  var Spot = sequelize.define('Spot', {
    number: DataTypes.STRING,
    latitude: DataTypes.DOUBLE,
    longitude: DataTypes.DOUBLE,
    address: DataTypes.STRING,
    description: DataTypes.TEXT,
    picture: DataTypes.STRING,
    price: DataTypes.DOUBLE
  }, {});
  Spot.associate = function(models) {
      Spot.belongsTo(models.User, {onDelete:'CASCADE',hooks: true, onUpdate: 'CASCADE',foreignKey: 'idUser'});
      Spot.belongsTo(models.Town, {foreignKey: 'idTown'});
      Spot.hasMany(models.Disponibility, {onDelete:'CASCADE',hooks: true, foreignKey: 'idSpot'});
      Spot.hasMany(models.Booking, {onDelete:'CASCADE',hooks: true, foreignKey: 'idSpot'});
  };
  return Spot;
};