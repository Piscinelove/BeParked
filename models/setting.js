'use strict';
module.exports = (sequelize, DataTypes) => {
  var Setting = sequelize.define('Setting', {
    key: DataTypes.STRING,
    text: DataTypes.TEXT,
  }, {});
  Setting.associate = function(models) {

  };
  return Setting;
};