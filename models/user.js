'use strict';

const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    address: DataTypes.STRING,
    sold: DataTypes.DECIMAL,
    brand: DataTypes.STRING,
    licensePlate: DataTypes.STRING,
    password_hash: DataTypes.TEXT,
    password :
        {
          type: DataTypes.VIRTUAL,
          set: function (val)
          {
              this.setDataValue('password', val);
              this.setDataValue('password_hash', bcrypt.hashSync(val, saltRounds));
          }
        }
  }, {});
  User.associate = function(models){
      User.belongsTo(models.Role, {onDelete: 'CASCADE', foreignKey: 'idRole'});
      User.belongsTo(models.Town, {foreignKey: 'idTown'});
      User.hasMany(models.Spot, {onDelete: 'CASCADE',foreignKey: 'idUser'});
      User.hasMany(models.Booking, {foreignKey: 'idUser'});
      User.hasMany(models.Message, {onDelete: 'CASCADE',foreignKey: 'from'});
      User.hasMany(models.Message, {onDelete: 'CASCADE',foreignKey: 'to'});
  }
  return User;
};