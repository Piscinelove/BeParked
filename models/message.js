'use strict';
module.exports = (sequelize, DataTypes) => {
  var Message = sequelize.define('Message', {
    content: DataTypes.TEXT,
    readAt: DataTypes.DATE
  }, {});
  Message.associate = function(models) {
      Message.belongsTo(models.User, {foreignKey: 'from'});
      Message.belongsTo(models.User, {foreignKey: 'to'});
  };
  return Message;
};