'use strict';
module.exports = (sequelize, DataTypes) => {
  var Booking = sequelize.define('Booking', {
    startHour: DataTypes.TIME,
    endHour: DataTypes.TIME,
    price: DataTypes.DOUBLE
  }, {});
  Booking.associate = function(models) {
      Booking.belongsTo(models.User, {foreignKey: 'idUser'});
      Booking.belongsTo(models.Spot, {foreignKey: 'idSpot'});
      Booking.belongsTo(models.Disponibility, {onDelete:'CASCADE',hooks: true,foreignKey: 'idDisponibility'});
  };
  return Booking;
};