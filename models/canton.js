'use strict';
module.exports = (sequelize, DataTypes) => {
  var Canton = sequelize.define('Canton', {
    name: DataTypes.STRING
  }, {});
  Canton.associate = function(models) {
      Canton.hasMany(models.Town, {foreignKey: 'idCanton'});
  };
  return Canton;
};